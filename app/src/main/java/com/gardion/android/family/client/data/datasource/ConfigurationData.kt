package com.gardion.android.family.client.data.datasource


interface ConfigurationData {
    fun saveConfigurationDeviceId(deviceId: String)
    fun getConfigurationDeviceId(): String?
    fun saveConfigurationDeviceName(deviceName: String)
    fun getConfigurationDeviceName(): String?
    fun saveConfigurationPkcs12(pkcs12Base64: String)
    fun getConfigurationPkcs12(): String?
    fun saveConfigurationUserCertificateAlias(alias: String)
    fun getConfigurationUserCertificateAlias(): String?
    fun saveConfigurationGroupId(groupId: String)
    fun getConfigurationGroupId(): String?
    fun saveConfigurationGroupName(groupName: String)
    fun getConfigurationGroupName(): String?
    fun saveConfigurationParentPin(parentPin: String)
    fun getConfigurationParentPin(): String?
    fun saveConfigurationVpnName(vpnName: String)
    fun getConfigurationVpnName(): String?
    fun saveConfigurationExcludedApps(apps: Set<String>?)
    fun getConfigurationExcludedApps(): Set<String>?
    fun saveConfigurationExcludedSubnets(subnets: Set<String>?)
    fun getConfigurationExcludedSubnets(): Set<String>?
    fun saveConfigurationUseParentPin(use: Boolean?)
    fun getConfigurationUseParentPin(): Boolean?
}