package com.gardion.android.family.client.data.datasource

import android.content.SharedPreferences
import com.gardion.android.family.client.get
import com.gardion.android.family.client.set
import org.strongswan.android.utils.StringUtils

class SharedPreferencesDataStore constructor(private val preferences: SharedPreferences) : DataStore {

    companion object {

        const val PREFERENCES_NAME = "gardion_shared_prefs"

        //Gardion flow data
        private const val ENCRYPTED_PASSWORD = "encrypted_password"
        private const val GLOBAL_PASS_SET = "global_pass_set"
        private const val VPN_PROFILE_SAVED = "vpn_profile_saved"
        private const val DEVICE_ADMIN_FIRST_SET = "device_admin_first_set"
        private const val GARDION_APP_UNLOCKED = "gardion_app_unlocked"
        private const val GARDION_FIRST_START = "gardion_first_start"
        private const val USER_CERTIFICATE_CHOSEN = "user_certificate_chosen"
        private const val USER_CERTIFICATE_USED = "user_certificate_used"
        private const val GARDION_DEACTIVATED_ALLOWED = "gardion_deactivated_allowed"
        private const val VPN_ACTIVITY_OPEN = "vpn_activity_open"
        private const val GARDION_SETUP_SUCCESSFUL = "gardion_setup_successful"
        private const val PUSH_NOTIFICATION_PREFERENCE_SET = "push_notification_preference_set"
        private const val SAFE_MODE_USED_CHECKED = "safe_mode_used_checked"
        private const val TAMPERING_MODE_ACTIVE = "tampering_mode_active"
        //Gardion configuration data
        private const val CONFIGURATION_GROUP_ID = "configuration_group_id"
        private const val CONFIGURATION_GROUP_NAME = "configuration_group_name"
        private const val CONFIGURATION_DEVICE_ID = "configuration_device_id"
        private const val CONFIGURATION_DEVICE_NAME = "configuration_device_name"
        private const val CONFIGURATION_PKCS12 = "configuration_pkcs12"
        private const val CONFIGURATION_USER_CERTIFICATE_ALIAS = "configuration_user_certificate_alias"
        private const val CONFIGURATION_PARENT_PIN = "configuration_parent_pin"
        private const val CONFIGURATION_VPN_NAME = "configuration_vpn_name"
        private const val CONFIGURATION_EXCLUDED_APPS = "configuration_excluded_apps"
        private const val CONFIGURATION_EXCLUDED_SUBNETS = "configuration_excluded_apps"
        private const val CONFIGURATION_USE_PARENT_PIN = "configuration_use_parent_pin"
        //Usage data
        private const val DEVICE_PICKUPS_TODAY = "device_picked_up_today"
        private const val DEVICE_LAST_SHUTDOWN = "device_last_shutdown"
    }

    override fun saveEncryptedPass(password: String) {
        preferences.set(ENCRYPTED_PASSWORD, password)
    }

    override fun getEncryptedPass(): String {
        return preferences[ENCRYPTED_PASSWORD] ?: StringUtils.EMPTY
    }

    override fun setGlobalPasswordCreated(passwordCreated: Boolean) {
        preferences.set(GLOBAL_PASS_SET, passwordCreated)
    }

    override fun isGlobalPasswordCreated(): Boolean? {
        return preferences[GLOBAL_PASS_SET, false]
    }

    override fun setVpnProfileSaved(vpnProfileSaved: Boolean) {
        preferences.set(VPN_PROFILE_SAVED, vpnProfileSaved)
    }

    override fun isVpnProfileSaved(): Boolean? {
        return preferences[VPN_PROFILE_SAVED, false]
    }

    override fun deviceAdminFirstSet(firstSet: Boolean) {
        preferences.set(DEVICE_ADMIN_FIRST_SET, firstSet)
    }

    override fun isDeviceAdminFirstSet(): Boolean? {
        return preferences[DEVICE_ADMIN_FIRST_SET, true]
    }

    override fun setGardionUnlocked(unlocked: Boolean) {
        preferences.set(GARDION_APP_UNLOCKED, unlocked)
    }

    override fun isGardionUnlocked(): Boolean? {
        return preferences[GARDION_APP_UNLOCKED, false]
    }

    override fun saveConfigurationGroupId(groupId: String) {
        preferences.set(CONFIGURATION_GROUP_ID, groupId)
    }

    override fun getConfigurationGroupId(): String? {
        return preferences[CONFIGURATION_GROUP_ID]
    }

    override fun saveConfigurationGroupName(groupName: String) {
        preferences.set(CONFIGURATION_GROUP_NAME, groupName)
    }

    override fun getConfigurationGroupName(): String? {
        return preferences[CONFIGURATION_GROUP_NAME]
    }

    override fun saveConfigurationDeviceId(deviceId: String) {
        preferences.set(CONFIGURATION_DEVICE_ID, deviceId)
    }

    override fun getConfigurationDeviceId(): String? {
        return preferences[CONFIGURATION_DEVICE_ID]
    }

    override fun saveConfigurationDeviceName(deviceName: String) {
        preferences.set(CONFIGURATION_DEVICE_NAME, deviceName)
    }

    override fun getConfigurationDeviceName(): String? {
        return preferences[CONFIGURATION_DEVICE_NAME]
    }

    override fun saveConfigurationPkcs12(pkcs12Base64: String) {
        preferences.set(CONFIGURATION_PKCS12, pkcs12Base64)
    }

    override fun getConfigurationPkcs12(): String? {
        return preferences[CONFIGURATION_PKCS12]
    }

    override fun saveConfigurationUserCertificateAlias(alias: String) {
        preferences.set(CONFIGURATION_USER_CERTIFICATE_ALIAS, alias)
    }

    override fun getConfigurationUserCertificateAlias(): String? {
        return preferences[CONFIGURATION_USER_CERTIFICATE_ALIAS]
    }

    override fun saveConfigurationParentPin(parentPin: String) {
        preferences.set(CONFIGURATION_PARENT_PIN, parentPin)
    }

    override fun getConfigurationParentPin(): String? {
        return preferences[CONFIGURATION_PARENT_PIN]
    }

    override fun saveConfigurationVpnName(vpnName: String) {
        preferences.set(CONFIGURATION_VPN_NAME, vpnName)
    }

    override fun getConfigurationVpnName(): String? {
        return preferences[CONFIGURATION_VPN_NAME]
    }

    override fun saveConfigurationExcludedApps(apps: Set<String>?) {
        preferences.set(CONFIGURATION_EXCLUDED_APPS, apps)
    }

    override fun getConfigurationExcludedApps(): Set<String>? {
        return preferences[CONFIGURATION_EXCLUDED_APPS]
    }

    override fun saveConfigurationExcludedSubnets(subnets: Set<String>?) {
        preferences.set(CONFIGURATION_EXCLUDED_SUBNETS, subnets)
    }

    override fun getConfigurationExcludedSubnets(): Set<String>? {
        return preferences[CONFIGURATION_EXCLUDED_SUBNETS]
    }

    override fun saveConfigurationUseParentPin(use: Boolean?) {
        preferences.set(CONFIGURATION_USE_PARENT_PIN, use)
    }

    override fun getConfigurationUseParentPin(): Boolean? {
        return preferences[CONFIGURATION_USE_PARENT_PIN, true]
    }

    override fun isGardionFirstStart(): Boolean? {
        return preferences[GARDION_FIRST_START, true]
    }

    override fun gardionFirstStart(firstStart: Boolean) {
        preferences.set(GARDION_FIRST_START, firstStart)
    }

    override fun isUserCertificateChosen(): Boolean? {
        return preferences[USER_CERTIFICATE_CHOSEN, false]
    }

    override fun userCertificateChosen(chosen: Boolean) {
        preferences.set(USER_CERTIFICATE_CHOSEN, chosen)
    }

    override fun isUserCertificateUsed(): Boolean? {
        return preferences[USER_CERTIFICATE_USED, false]
    }

    override fun userCertificateUsed(certificate: Boolean) {
        preferences.set(USER_CERTIFICATE_USED, certificate)
    }

    override fun isGardionDeactivatedAllowed(): Boolean? {
        return preferences[GARDION_DEACTIVATED_ALLOWED]
    }

    override fun gardionDeactivatedAllowed(deactivated: Boolean) {
        preferences.set(GARDION_DEACTIVATED_ALLOWED, deactivated)
    }

    override fun isVpnActivityOpen(): Boolean? {
        return preferences[VPN_ACTIVITY_OPEN, false]
    }

    override fun vpnActivityOpen(open: Boolean) {
        preferences.set(VPN_ACTIVITY_OPEN, open)
    }

    override fun isGardionSetupSuccessful(): Boolean? {
        return preferences[GARDION_SETUP_SUCCESSFUL, false]
    }

    override fun gardionSetupSuccessful(successful: Boolean) {
        preferences.set(GARDION_SETUP_SUCCESSFUL, successful)
    }

    override fun getDevicePickupsToday(): Int {
        return preferences[DEVICE_PICKUPS_TODAY, 0] ?: 0
    }

    override fun saveDevicePickupsToday(value: Int) {
        preferences.set(DEVICE_PICKUPS_TODAY, value)
    }

    override fun isPushNotificationPreferenceSet(): Boolean? {
        return preferences[PUSH_NOTIFICATION_PREFERENCE_SET]
    }

    override fun pushNotificationPreferenceSet(set: Boolean) {
        preferences.set(PUSH_NOTIFICATION_PREFERENCE_SET, set)
    }

    override fun getDeviceLastShutdown(): Long? {
        return preferences[DEVICE_LAST_SHUTDOWN]
    }

    override fun saveDeviceLastShutdown(time: Long) {
        preferences.set(DEVICE_LAST_SHUTDOWN, time)
    }

    override fun isSafeModeUsedChecked(): Boolean? {
        return preferences[SAFE_MODE_USED_CHECKED]
    }

    override fun safeModeUsedChecked(checked: Boolean) {
        preferences.set(SAFE_MODE_USED_CHECKED, checked)
    }

    override fun isTamperingModeActive(): Boolean? {
        return preferences[TAMPERING_MODE_ACTIVE]
    }

    override fun tamperingModeActive(active: Boolean) {
        preferences.set(TAMPERING_MODE_ACTIVE, active)
    }

}