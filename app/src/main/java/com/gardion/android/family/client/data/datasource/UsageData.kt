package com.gardion.android.family.client.data.datasource

interface UsageData {
    fun saveDevicePickupsToday(value: Int)
    fun getDevicePickupsToday(): Int
    fun saveDeviceLastShutdown(time: Long)
    fun getDeviceLastShutdown(): Long?
}