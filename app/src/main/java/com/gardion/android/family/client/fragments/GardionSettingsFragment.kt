package com.gardion.android.family.client.fragments

import android.os.Bundle
import androidx.preference.*
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.GardionApplication
import com.gardion.android.family.client.network.PushNotificationService


open class GardionSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        return when (preference?.key) {
            getString(R.string.pref_key_push_notification_enabled) -> {
                if ((preference as SwitchPreferenceCompat).isChecked) {
                    PushNotificationService.sendTokenToServer(GardionApplication.applicationContext())
                } else {
                    PushNotificationService.sendNullTokenToServer(GardionApplication.applicationContext())
                }
                true
            }
            else -> {
                super.onPreferenceTreeClick(preference)
            }
        }
    }
}