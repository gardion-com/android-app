package com.gardion.android.family.client.fragments

import android.os.Bundle
import com.gardion.android.family.client.R


class GardionSettingsPushFragment : GardionSettingsFragment() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.push_preferences, rootKey)
    }
}