package com.gardion.android.family.client.logic

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

class DevicePickupsResetService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("GARDION", "DevicePickupsResetService")

        UsageStats.resetDevicePickupsToday()
        stopSelf()
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


}