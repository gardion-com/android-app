package com.gardion.android.family.client.logic

import android.app.Activity
import android.app.AppOpsManager
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.ui.*
import com.gardion.android.family.client.receiver.GardionDeviceAdminReceiver
import com.gardion.android.family.client.utils.ControlWorkers
import com.gardion.android.family.client.utils.NotificationManager
import com.gardion.android.family.client.utils.Utils
import com.gardion.android.family.client.utils.Utils.Companion.hasOverlayPermission


class FlowController : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE_ENABLE_ADMIN: Int = 200
        private const val REQUEST_EXPLAIN_ADMIN_SCREEN: Int = 101
        private const val REQUEST_GARDION_LOGIN: Int = 103
        private const val REQUEST_VPN_START: Int = 104
        private const val REQUEST_WELCOME_SCREEN: Int = 105
        private const val REQUEST_IMPORT_CERTIFICATE: Int = 107
        private const val REQUEST_SHOW_PARENT_PIN: Int = 108
        private const val REQUEST_EXPLAIN_USAGE_STATS_SCREEN: Int = 109
        private const val REQUEST_USAGE_STATS_PERMISSION: Int = 110
        private const val REQUEST_PUSH_NOTIFICATION_PREFERENCES: Int = 111
        private const val REQUEST_EXPLAIN_OVERLAY_PERMISSION: Int = 112
        private const val REQUEST_OVERLAY_PERMISSION: Int = 113

        const val INTENT_PARENT_PIN: String = "intent_parent_pin"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val gardionNotificationManager = NotificationManager(this)
            gardionNotificationManager.createAllNotificationChannels()
        }

        addShareShortcuts(this)
        ControlWorkers.stopGardionActiveWorkers(this)
        ControlWorkers.startGardionActiveWorkers(this)

        when {
            flowData.isGardionFirstStart()!! -> startWelcomeScreen()
            !Utils.isDeviceAdminActive(this) -> startExplainAdminScreen()
            !Utils.hasUsageStatsPermission(this) -> startExplainUsageStatsScreen()
            !flowData.isVpnProfileSaved()!! -> startGardionLogin()
            flowData.isUserCertificateUsed()!! && !flowData.isUserCertificateChosen()!! ->
                startCertificateInstallation()
            !flowData.isPushNotificationPreferenceSet()!! -> startPushNotificationPreferences()
            !hasOverlayPermission(this) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ->
                startExplainOverlayPermission()
            else -> startVpnService()
        }
    }

    private fun startWelcomeScreen() {
        startActivityForResult(WelcomeActivity.getIntent(this),
                REQUEST_WELCOME_SCREEN)
    }

    private fun startExplainAdminScreen() {
        startActivityForResult(AdminActivity.getIntent(this),
                REQUEST_EXPLAIN_ADMIN_SCREEN)
    }

    private fun startShowParentPin(parentPin: String?) {
        val intent = ShowParentPinActivity.getIntent(this)
        intent.putExtra(INTENT_PARENT_PIN, parentPin)
        startActivityForResult(intent, REQUEST_SHOW_PARENT_PIN)
    }

    private fun startCertificateInstallation() {
        startActivityForResult(CertificateActivity.getIntent(this),
                REQUEST_IMPORT_CERTIFICATE)
    }

    private fun startExplainUsageStatsScreen() {
        startActivityForResult(GardionUsageStatsActivity.getIntent(this),
                REQUEST_EXPLAIN_USAGE_STATS_SCREEN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_WELCOME_SCREEN -> handleWelcomeScreen()
            REQUEST_EXPLAIN_ADMIN_SCREEN -> handleExplainAdminScreen()
            REQUEST_CODE_ENABLE_ADMIN -> handleDeviceAdminCreation()
            REQUEST_SHOW_PARENT_PIN -> handleShowParentPin()
            REQUEST_EXPLAIN_USAGE_STATS_SCREEN -> handleUsageStatsScreen()
            REQUEST_USAGE_STATS_PERMISSION -> handleUsageStatsPermission()
            REQUEST_GARDION_LOGIN -> handleGardionLogin(data, resultCode)
            REQUEST_IMPORT_CERTIFICATE -> handleGardionCertificate(resultCode)
            REQUEST_PUSH_NOTIFICATION_PREFERENCES -> handlePushNotificationPreferences()
            REQUEST_VPN_START -> handleVpnStart()
            REQUEST_EXPLAIN_OVERLAY_PERMISSION ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    handleExplainOverlayPermission()
                }
            REQUEST_OVERLAY_PERMISSION -> handleOverlayPermission()
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleGardionCertificate(resultCode: Int) {
        if (resultCode == Activity.RESULT_OK) {
            flowData.userCertificateChosen(true)
            startPushNotificationPreferences()
        } else {
            flowData.userCertificateChosen(false)
            finish()
        }
    }

    private fun handleWelcomeScreen() {
        flowData.gardionFirstStart(false)
        if (!Utils.isDeviceAdminActive(this)) {
            startExplainAdminScreen()
        } else {
            handleDeviceAdminCreation()
        }
    }

    private fun handleExplainAdminScreen() {
        askForDeviceAdmin()
    }

    private fun handleVpnStart() {
        finish()
    }

    private fun handleShowParentPin() {
        flowData.setGlobalPasswordCreated(true)
        if (flowData.isUserCertificateUsed()!! && !flowData.isUserCertificateChosen()!!) {
            startCertificateInstallation()
        } else {
            startVpnService()
        }
    }

    private fun handleGardionLogin(data: Intent?, resultCode: Int) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                flowData.userCertificateUsed(
                        data.extras!!.getBoolean(LoginActivity.INTENT_CERTIFICATE_USED)
                )
                if (configData.getConfigurationUseParentPin()!!) {
                    startShowParentPin(
                            data.extras!!.getString(LoginActivity.INTENT_PARENT_PIN)
                    )
                } else {
                    handleShowParentPin()
                    // TODO : or finish()?
                }
            } else {
                flowData.userCertificateUsed(false)
                finish()
            }
        } else {
            finish()
        }
    }

    private fun startPushNotificationPreferences() {
        startActivityForResult(PushNotificationActivity.getIntent(this),
                REQUEST_PUSH_NOTIFICATION_PREFERENCES)
    }

    private fun startVpnService() {
        startActivityForResult(GardionVpnActivity.getIntent(this), REQUEST_VPN_START)
    }


    private fun startGardionLogin() {
        startActivityForResult(LoginActivity.getIntent(this), REQUEST_GARDION_LOGIN)
    }

    private fun startExplainOverlayPermission() {
        startActivityForResult(OverlayPermissionActivity.getIntent(this),
                REQUEST_EXPLAIN_OVERLAY_PERMISSION)

    }

    private fun askForDeviceAdmin() {
        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                GardionDeviceAdminReceiver.getComponentName(this))
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                getString(R.string.device_admin_extra_explanation))
        startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN)
    }

    private fun handleUsageStatsScreen() {
        askForSpecialPermission(this, AppOpsManager.OPSTR_GET_USAGE_STATS,
                Settings.ACTION_USAGE_ACCESS_SETTINGS, REQUEST_USAGE_STATS_PERMISSION)
    }

    private fun handleDeviceAdminCreation() {
        if (!Utils.hasUsageStatsPermission(this)) {
            startExplainUsageStatsScreen()
        } else {
            handleUsageStatsPermission()
        }
    }

    private fun handleUsageStatsPermission() {
        when {
            flowData.isUserCertificateChosen()!! -> startVpnService()
            flowData.isVpnProfileSaved()!! -> startCertificateInstallation()
            else -> startGardionLogin()
        }
    }

    private fun handlePushNotificationPreferences() {
        flowData.pushNotificationPreferenceSet(true)
        // on this versions we need the SYSTEM_ALERT_WINDOW permission for PopupBlockActivity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !hasOverlayPermission(this)) {
            startExplainOverlayPermission()
        } else {
            startVpnService()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun handleExplainOverlayPermission() {
        askForSpecialPermission(this, AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION, REQUEST_OVERLAY_PERMISSION)
    }

    private fun handleOverlayPermission() {
        startVpnService()
    }

    // ask for a permission; permission must be a string constant of AppOpsManager, setting
    // must be the corresponding Settings string, requestCode must be defined in FlowController
    private fun askForSpecialPermission(context: Context, permission: String, setting: String,
                                        requestCode: Int) {
        val appOpsManager = getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

        appOpsManager.startWatchingMode(permission,
                applicationContext.packageName,
                object : AppOpsManager.OnOpChangedListener {
                    override fun onOpChanged(op: String, packageName: String) {
                        val modePermission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            appOpsManager.unsafeCheckOpNoThrow(permission,
                                    android.os.Process.myUid(), getPackageName())
                        } else {
                            @Suppress("DEPRECATION")
                            appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                                    android.os.Process.myUid(), getPackageName())
                        }

                        if (modePermission != AppOpsManager.MODE_ALLOWED) {
                            return
                        }

                        appOpsManager.stopWatchingMode(this)

                        val intent = Intent(context, FlowController::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        applicationContext.startActivity(intent)
                    }
                }
        )

        val intent = Intent(setting)
        // seems to only work for Q, at least some devices (emulator) have problems with this
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.data = Uri.fromParts("package", packageName, null)
        }
        startActivityForResult(intent, requestCode)
    }

    private fun addShareShortcuts(context: Context) {
        val shortcutInfoList = mutableListOf<ShortcutInfoCompat>()
        val categories = setOf("REPORT_URL")
        val intent = Intent(this, ReportUrl::class.java)
        intent.action = Intent.ACTION_SEND
        shortcutInfoList.add(
                ShortcutInfoCompat.Builder(context, "111")
                        .setShortLabel("Website gesperrt")
                        .setIcon(IconCompat.createWithResource(context, R.mipmap.ic_launcher))
                        .setCategories(categories)
                        .setIntent(intent)
                        .build())
        ShortcutManagerCompat.addDynamicShortcuts(context, shortcutInfoList)
    }
}