package com.gardion.android.family.client.logic

import android.app.Application
import android.content.Context
import com.gardion.android.family.client.data.datasource.ConfigurationData
import com.gardion.android.family.client.data.datasource.FlowData
import com.gardion.android.family.client.data.datasource.SharedPreferencesDataStore
import com.gardion.android.family.client.data.datasource.UsageData
import com.gardion.android.family.client.security.LocalCertificateKeyStoreProvider
import java.security.Security

class GardionApplication : Application() {

    init {
        instance = this
        Security.addProvider(LocalCertificateKeyStoreProvider())
        System.loadLibrary("androidbridge")
    }

    companion object {
        private var instance: GardionApplication? = null
        lateinit var flowData: FlowData
        lateinit var configData: ConfigurationData
        lateinit var usageData: UsageData

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        applicationContext()
        val prefs = getSharedPreferences(SharedPreferencesDataStore.PREFERENCES_NAME, Context.MODE_PRIVATE)
        flowData = SharedPreferencesDataStore(prefs)
        configData = SharedPreferencesDataStore(prefs)
        usageData = SharedPreferencesDataStore(prefs)
    }
}