package com.gardion.android.family.client.logic

import android.content.*
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.receiver.*
import com.gardion.android.family.client.utils.NotificationManager
import com.gardion.android.family.client.utils.EventManager
import org.strongswan.android.logic.CharonVpnService


// TODO - move (own) build notification here?
class GardionVpnService : CharonVpnService() {

    companion object {
        private const val LOG_TAG = "GARDION_VPN"
        const val DISCONNECT_ACTION = "org.strongswan.android.CharonVpnService.DISCONNECT"
    }

    private val userPresentReceiver = UserPresentReceiver()
    private val packageAddedReceiver = PackageAddedReceiver()
    private val powerDisconnectedReceiver = PowerDisconnectedReceiver()
    private val keychainChangedReceiver = KeychainChangedReceiver()
    private val userSwitchedReceiver = UserSwitchedReceiver()
    private val shutdownReceiver = ShutdownReceiver()

    override fun onCreate() {
        super.onCreate()
        // registering UserPresent is necessary as of Android API 26 (implicit broadcast receiver)
        // doing this here in VpnService to persist destroying of app
        registerAllBroadcastReceivers(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notificationManager = NotificationManager(this)
        // required when using startForegroundService
        startForeground(
                notificationManager.getNotificationId(
                        NotificationManager.GardionNotificationType.START_VPN_FOREGROUND
                ),
                notificationManager.createNotification(
                        NotificationManager.GardionNotificationType.START_VPN_FOREGROUND,
                        null,
                        null))
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onRevoke() {
        super.onRevoke()
        Log.d(LOG_TAG, "onRevoke")
        flowData.tamperingModeActive(true)
        val eventManager = EventManager(this)
        eventManager.sendGardionEvent(EventManager.GardionEventType.VPN_REMOVED)
        val intent = Intent(applicationContext, FlowController::class.java)
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterAllBroadcastReceivers(this)
        val notificationManager = NotificationManager(this)
        notificationManager.cancelNotification(NotificationManager.GardionNotificationType.START_VPN_FOREGROUND)
    }

    private fun registerAllBroadcastReceivers(context: Context) {
        registerBroadcastReceiver(context, userPresentReceiver,
                arrayOf("android.intent.action.USER_PRESENT"))
        registerBroadcastReceiver(context, packageAddedReceiver,
                arrayOf("android.intent.action.PACKAGE_ADDED"), "package")
        registerBroadcastReceiver(context, powerDisconnectedReceiver,
                arrayOf("android.intent.context.ACTION_POWER_DISCONNECTED"))
        registerBroadcastReceiver(context, keychainChangedReceiver,
                arrayOf("android.security.action.KEYCHAIN_CHANGED", "android.security.STORAGE_CHANGED"))
        registerBroadcastReceiver(context, userSwitchedReceiver,
                arrayOf("android.intent.action.USER_BACKGROUND"))
        registerBroadcastReceiver(context, shutdownReceiver,
                arrayOf("android.intent.action.ACTION_SHUTDOWN"))
    }

    private fun unregisterAllBroadcastReceivers(context: Context) {
        unregisterBroadcastReceiver(context, userPresentReceiver)
        unregisterBroadcastReceiver(context, packageAddedReceiver)
        unregisterBroadcastReceiver(context, powerDisconnectedReceiver)
        unregisterBroadcastReceiver(context, keychainChangedReceiver)
        unregisterBroadcastReceiver(context, userSwitchedReceiver)
        unregisterBroadcastReceiver(context, shutdownReceiver)
    }

    // using array here instead of varargs to avoid problems with following optional parameter
    private fun registerBroadcastReceiver(context: Context, receiver: BroadcastReceiver,
                                          intentActions: Array<String>,
                                          intentDataScheme: String? = null) {
        val intentFilter = IntentFilter()
        intentActions.forEach { action ->
            intentFilter.addAction(action)
            Log.d(LOG_TAG, "registered receiver $action")
        }
        if (intentDataScheme != null) intentFilter.addDataScheme(intentDataScheme)
        context.registerReceiver(receiver, intentFilter)
    }

    private fun unregisterBroadcastReceiver(context: Context, receiver: BroadcastReceiver) {
        try {
            context.unregisterReceiver(receiver)
            Log.d(LOG_TAG, "unregistered receiver $receiver")
        } catch (e: Exception) {
            Log.d(LOG_TAG, "$e / no registered receiver $receiver found to unregister")
        }
    }
}