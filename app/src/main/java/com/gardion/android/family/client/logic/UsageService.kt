package com.gardion.android.family.client.logic

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.work.WorkManager
import com.gardion.android.family.client.utils.ControlWorkers
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.Utils
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

// send out usage stats data
// note: as we want to send out data every 5 minutes we cannot just use a periodic worker
class UsageService : Service(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var job: Job
    private val binder: IBinder = ConnectionLocalBinder()
    private var disposable: Disposable? = null

    private val logTag = "GARDION_STATS"

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(logTag, "${javaClass.simpleName} started")
        disposable = Observable.interval(5, TimeUnit.MINUTES)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { postUsageStats(this) }
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        job = Job()
    }

    private fun postUsageStats(context: Context) {
        val eventManager = EventManager(context)

        if (Utils.hasUsageStatsPermission(context)) {
            WorkManager.getInstance(context).cancelAllWorkByTag("usage_stats")
            ControlWorkers.startUsageStatsWorker(context)
        } else {
            eventManager.sendGardionEvent(
                    EventManager.GardionEventType.NO_USAGE_STATS_PERMISSION,
                    null)
        }
    }

    override fun onBind(p0: Intent?): IBinder {
        return binder
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    class ConnectionLocalBinder : Binder() {
        fun getService(): UsageService {
            return UsageService()
        }
    }
}