package com.gardion.android.family.client.logic

import android.app.usage.UsageEvents
import android.app.usage.UsageStatsManager
import android.content.Context
import android.os.Build
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication.Companion.usageData
import com.google.gson.JsonObject
import kotlin.collections.MutableMap
import com.gardion.android.family.client.utils.Utils


class UsageStats {

    companion object {

        val event_foreground = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            UsageEvents.Event.ACTIVITY_RESUMED
        } else {
            @Suppress("DEPRECATION")
            UsageEvents.Event.MOVE_TO_FOREGROUND
        }

        private val event_background = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            UsageEvents.Event.ACTIVITY_PAUSED
        } else {
            @Suppress("DEPRECATION")
            UsageEvents.Event.MOVE_TO_BACKGROUND
        }

        // TODO - check if we want to switch the format of output
        fun getUsageStatsTodayJson(context: Context): JsonObject {
            val statsToday = getStatsToday(context)
            val timeOverallToday = getOverallTime(statsToday)

            val statsTodayJson = JsonObject()
            statsToday.forEach {
                if (it.totalTime > 0) {
//                    val statsPackage = JsonObject()
//                    statsPackage.addProperty("app_label",
//                            Utils.getAppLabel(context, it.packageName))
                    statsTodayJson.addProperty(
                            it.packageName,
                            when (it.totalTime) {
                                is Int -> it.totalTime
                                else -> 0
                            })
                }
            }

            //TODO - add data class?
            val usageStats = JsonObject()
            usageStats.addProperty("time_overall_today", timeOverallToday)
            usageStats.add("time_per_package", statsTodayJson)
            usageStats.addProperty("device_pickups_today", usageData.getDevicePickupsToday())
            Log.d("GARDION_USAGE", usageStats.toString())
            return (usageStats)
        }

        private fun getStats(context: Context, startTime: Long, endTime: Long, startTimeDefault: Long, endTimeDefault: Long): MutableList<Stat> {
            return calculateStats(sortEvents(getEvents(context, startTime, endTime)), startTimeDefault, endTimeDefault)
        }

        private fun getStatsToday(context: Context): MutableList<Stat> {
            return getStats(context, Utils.calStartOfToday().timeInMillis, System.currentTimeMillis(),
                    Utils.calStartOfToday().timeInMillis, System.currentTimeMillis())
        }

        private fun getOverallTime(stats: MutableList<Stat>): Int {
            return stats.sumBy { it.totalTime }
        }

        fun getEvents(context: Context, startTime: Long, endTime: Long): UsageEvents {
            val statsManager = getStatsManager(context)
            return statsManager.queryEvents(startTime, endTime)
        }

        private fun sortEvents(events: UsageEvents): MutableMap<String, MutableList<UsageEvents.Event>> {
            val sortedEvents = mutableMapOf<String, MutableList<UsageEvents.Event>>()
            while (events.hasNextEvent()) {
                val event = UsageEvents.Event()
                events.getNextEvent(event)
                val packageEvents = sortedEvents[event.packageName] ?: mutableListOf()
                packageEvents.add(event)
                sortedEvents[event.packageName] = packageEvents
            }
            return (sortedEvents)
        }

        private fun calculateStats(sortedEvents: MutableMap<String, MutableList<UsageEvents.Event>>,
                                   startTimeDefault: Long, endTimeDefault: Long): MutableList<Stat> {
            val stats = mutableListOf<Stat>()
            sortedEvents.forEach { (packageName, events) ->
                var startTime = 0L
                var endTime = 0L
                var totalTime = 0
                val maxIndex = events.size - 1

                events.forEachIndexed { index, event ->
                    //first two cases are edge cases for first and last element
                    //does not use events where corresponding element (with type foreground / background) is missing
                    when {
                        index == 0 && event.eventType == event_background -> startTime = startTimeDefault
                        index == maxIndex && event.eventType == event_foreground -> endTime = endTimeDefault
                        else -> when (event.eventType) {
                            event_foreground -> startTime = event.timeStamp
                            event_background ->
                                if (endTime == 0L) endTime = event.timeStamp
                        }
                    }

                    //calculate time if both start and end time are set
                    if (startTime != 0L && endTime != 0L) {
                        totalTime += (endTime - startTime).toInt()
                        startTime = 0L
                        endTime = 0L
                    }
                }
                stats.add(Stat(packageName, totalTime))
            }
            return stats
        }

        @SuppressWarnings("WrongConstant") //needed for Android level 21 - else path
        private fun getStatsManager(context: Context): UsageStatsManager {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            } else {
                context.getSystemService("usagestats") as UsageStatsManager
            }
        }

        fun resetDevicePickupsToday() {
            usageData.saveDevicePickupsToday(0)
        }

        fun increaseDevicePickupsToday(value: Int) {
            usageData.saveDevicePickupsToday(usageData.getDevicePickupsToday() + value)
        }
    }

    class Stat(val packageName: String, val totalTime: Int)
}
