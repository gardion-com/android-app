package com.gardion.android.family.client.logic

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.EventManager.GardionEventType

class UsageStatsWorker(context: Context, workerParams: WorkerParameters) :
        Worker(context, workerParams) {

    companion object {
        private const val LOG_TAG = "GARDION_WORKER"
    }

    override fun doWork(): Result {
        Log.d(LOG_TAG, "do Work")
        val usageStats = UsageStats.getUsageStatsTodayJson(applicationContext)
        val eventManager = EventManager(applicationContext)
        val sendSuccess = eventManager.sendEventForWorker(
                GardionEventType.USAGE_STATS, usageStats)
        return if (sendSuccess) {
            Result.success()
        } else {
            Result.retry()
        }
    }
}