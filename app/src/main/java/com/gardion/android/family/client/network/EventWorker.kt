package com.gardion.android.family.client.network

import android.content.Context
import android.util.Log
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.EventManager.GardionEventType
import com.google.gson.JsonNull
import com.google.gson.JsonObject

class EventWorker(context: Context, workerParams: WorkerParameters) :
        Worker(context, workerParams) {

    companion object {
        private const val LOG_TAG = "EVENT_WORKER"
    }

    override fun doWork(): Result {
        Log.d(LOG_TAG, "do Work")
        val eventId = inputData.getString("event_id")
        return if (eventId != null) {
            val eventType: GardionEventType = EventManager(applicationContext)
                    .getEventType(eventId)

            val eventManager = EventManager(applicationContext)
            val sendSuccess = eventManager.sendEventForWorker(eventType, input2Json(inputData))

            if (sendSuccess) {
                Result.success()
            } else {
                Result.retry()
            }
        } else {
            Log.e(LOG_TAG, "no event id specified")
            Result.failure()
        }
    }

    private fun input2Json(input: Data): JsonObject {
        val data: Map<String, *> = input.keyValueMap.filterKeys { !it.contains("event_id") }
        val payload = JsonObject()
        data.forEach {
            if (it.value != null) {
                payload.addProperty(it.key, it.value.toString())
            } else {
                payload.add(it.key, JsonNull.INSTANCE)
            }
        }
        return payload
    }
}