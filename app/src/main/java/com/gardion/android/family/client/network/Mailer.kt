package com.gardion.android.family.client.network

import android.content.Context
import android.content.Intent

//not used at the moment but left here for possible future implementation

class Mailer(private val context: Context) {
    private fun sendMail(recipients: Array<String>, subject: String) {
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_EMAIL, recipients)
        email.putExtra(Intent.EXTRA_SUBJECT, subject)
        email.type = "message/rfc822"
        email.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(Intent.createChooser(email, "Choose how to send the email"))
    }

    fun sendMailToSupport() {
        val toSupport = arrayOf("support@gardion.com")
        val subjectSupport = "Gardion: Support Android"
        this.sendMail(toSupport, subjectSupport)
    }
}
