package com.gardion.android.family.client.network

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.net.NetworkCapabilities
import android.util.Log
import com.gardion.android.family.client.ui.GardionVpnActivity

class Network {
    companion object {
        //TODO - best way to achieve the desired behavior? alt: registerNetworkCallback
        fun requestNetworkInternet(context: Context) {
            val manager = context.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val request = NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .build()
            val callback: ConnectivityManager.NetworkCallback = object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    Log.d("GARDION_CONNECTION", "onAvailable")
                    onAvailableNetwork(context)
                    manager.unregisterNetworkCallback(this)
                    super.onAvailable(network)
                }
            }
            manager.requestNetwork(request, callback)
        }

        private fun onAvailableNetwork(context: Context) {
            Log.d("GARDION_CONNECTION", "startVPN")
            val gardionIntent = Intent(context, GardionVpnActivity::class.java)
            gardionIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            gardionIntent.putExtra(GardionVpnActivity.KEY_IS_FROM_NETWORK_AVAILABLE, true)
            context.applicationContext?.startActivity(gardionIntent)
        }
    }
}