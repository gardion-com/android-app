package com.gardion.android.family.client.network

import android.content.Context
import android.util.Log
import com.gardion.android.family.client.utils.NotificationManager
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.Utils
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class PushNotificationService : FirebaseMessagingService() {

    companion object {
        private const val LOG_TAG = "PUSH_NOTIFICATIONS"

        // TODO - as this is kind of critical, we should check if this was successful and try again if not
        //  maybe via worker or just trying to submit (update) token with every connection to VPN
        // get push notification token from device and send to backend
        fun sendTokenToServer(context: Context) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                val token = instanceIdResult.token
                Log.d(LOG_TAG, "token: $token")
                val eventManager = EventManager(context)
                val tokenPayload = mapOf("push_token" to token)
                eventManager.sendGardionEvent(
                        EventManager.GardionEventType.PUSH_NOTIFICATION_TOKEN,
                        tokenPayload
                )
            }
        }

        fun sendNullTokenToServer(context: Context) {
            val eventManager = EventManager(context)
            val tokenPayload = mapOf("push_token" to null)
            eventManager.sendGardionEvent(
                    EventManager.GardionEventType.PUSH_NOTIFICATION_TOKEN,
                    tokenPayload
            )
        }
    }

    override fun onNewToken(token: String) {
        Log.d(LOG_TAG, "new token: $token")
        if (Utils.isPushNotificationEnabled(this)) {
            sendTokenToServer(this)
        }
    }

    // TODO - we must (later) differentiate between a message containing a notification and one containing
    //  data (e.g. asking the app to do something), for now we assume there are just notifications
    // TODO - check if title and body is null - in this case don't create a notification
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(LOG_TAG, "received message with title ${remoteMessage.notification?.title}")
        // show notification also if app is in foreground

        val notificationManager = NotificationManager(this)
        notificationManager.notify(
                NotificationManager.GardionNotificationType.PUSH_NOTIFICATION,
                remoteMessage.notification?.title ?: "",
                remoteMessage.notification?.body ?: "")
    }
}