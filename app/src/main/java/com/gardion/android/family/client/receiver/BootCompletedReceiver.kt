package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.ui.GardionVpnActivity
import com.gardion.android.family.client.utils.ReceiverUtils

class BootCompletedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("GARDION_RECEIVER", "BOOT_COMPLETED")
        if (
                intent?.action == Intent.ACTION_BOOT_COMPLETED ||
                intent?.action == Intent.ACTION_REBOOT ||
                intent?.action == "com.htc.action.QUICKBOOT_POWERON" ||
                intent?.action == "android.intent.action.QUICKBOOT_POWERON"
        ) {
            ReceiverUtils.connectVpnOnReceive(context, GardionVpnActivity.KEY_IS_FROM_BOOT_RECEIVER)
        }
    }
}