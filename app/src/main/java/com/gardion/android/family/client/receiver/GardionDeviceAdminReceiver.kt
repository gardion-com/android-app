package com.gardion.android.family.client.receiver

import android.app.admin.DeviceAdminReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.security.CheckAdminService
import com.gardion.android.family.client.toast
import com.gardion.android.family.client.utils.NotificationManager


class GardionDeviceAdminReceiver : DeviceAdminReceiver() {

    private var logTag = this::class.java.name

    companion object {
        fun getComponentName(context: Context): ComponentName {
            return ComponentName(context.applicationContext, GardionDeviceAdminReceiver::class.java)
        }
    }

    override fun onEnabled(context: Context, intent: Intent?) {
        stopCheckAdminService(context)
        informServerAboutActivation(context)
        cancelNotificationAboutDeactivation(context)
        context.toast(context.getString(R.string.device_admin_toast_active))
        Log.d(logTag, "Device admin enabled")
    }

    override fun onDisabled(context: Context, intent: Intent?) {
        startCheckAdminService(context)
        informServerAboutDeactivation(context)
        notifyAboutDeactivation(context)
        context.toast(context.getString(R.string.device_admin_toast_deactivated))
        Log.d(logTag, "Device admin disabled")
    }

    override fun onDisableRequested(context: Context, intent: Intent?): CharSequence {
        return if (flowData.isGardionSetupSuccessful()!!) {
            context.getString(R.string.device_admin_warning_deactivation)
        } else {
            context.getString(R.string.device_admin_explanation_deactivation)
        }
    }

    private fun startCheckAdminService(context: Context) {
        if (flowData.isGardionSetupSuccessful()!!) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(Intent(context.applicationContext, CheckAdminService::class.java))
            } else {
                context.startService(Intent(context.applicationContext, CheckAdminService::class.java))
            }
        }
    }

    private fun stopCheckAdminService(context: Context) {
        context.stopService(Intent(context.applicationContext, CheckAdminService::class.java))
    }

    private fun informServerAboutActivation(context: Context) {
        val eventManager = EventManager(context)
        eventManager.sendGardionEvent(EventManager.GardionEventType.ADMIN_ACTIVATION)
    }

    private fun informServerAboutDeactivation(context: Context) {
        val eventManager = EventManager(context)
        eventManager.sendGardionEvent(EventManager.GardionEventType.ADMIN_DEACTIVATION)
    }

    private fun notifyAboutDeactivation(context: Context) {
        val notificationManager = NotificationManager(context)
        notificationManager.notify(NotificationManager.GardionNotificationType.ADMIN_DEACTIVATED,
                null, null)
    }

    private fun cancelNotificationAboutDeactivation(context: Context) {
        val gardionNotificationManager = NotificationManager(context)
        gardionNotificationManager.cancelNotification(NotificationManager.GardionNotificationType.ADMIN_DEACTIVATED)
    }
}