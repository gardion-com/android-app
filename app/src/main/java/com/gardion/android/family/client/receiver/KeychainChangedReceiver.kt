package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.utils.EventManager

class KeychainChangedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent?.action == "android.security.STORAGE_CHANGED" || intent?.action == "android.security.action.KEYCHAIN_CHANGED") {
            Log.d("GARDION_CONNECTION", "receiver ${javaClass.simpleName}")

            // TODO - atm this acts upon every change in the Keychain not only on Gardion entries
            // the receiver is therefor not registered ("activated") for now
            // see also https://gitlab.com/gardion-com/android-app/issues/58

            if (flowData.isGardionSetupSuccessful()!!) {
                Log.d("GARDION_CONNECTION", "Certificate removed after successful setup")
                val eventManager = EventManager(context)
                eventManager.sendGardionEvent(EventManager.GardionEventType.CERTIFICATE_REMOVED)

//            val notificationManager = NotificationManager(context)
//            notificationManager.notify(NotificationManager.GardionNotificationType.CERTIFICATE_REMOVED)
//
//            flowData.userCertificateChosen(false)
//            flowData.gardionSetupSuccessful(false)
//
//            val intentFlow = Intent(context, FlowController::class.java)
//            intentFlow.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            context.applicationContext.startActivity(intentFlow)
                // TODO - disconnect Gardion with disabled illegal to make clear Gardion is not working properly anymore?
            } else {
                Log.d("GARDION_CONNECTION", "Change in Keychain but setup was never successfully finished.")
            }
        }
    }
}