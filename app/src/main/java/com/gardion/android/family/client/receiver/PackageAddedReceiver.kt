package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.Utils

class PackageAddedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d("GARDION_RECEIVER", "${this::class.java.simpleName}: broadcast ACTION_PACKAGE_ADDED")
        if (intent?.action == Intent.ACTION_PACKAGE_ADDED) {
            // check if this was just an update
            if (!intent.getBooleanExtra(Intent.EXTRA_REPLACING, false)) {
                val packageName = intent.data?.toString()?.replace("package:", "")
                val appLabel = Utils.getAppLabel(context, packageName!!)
                Log.d("GARDION_RECEIVER", "Package was added: $packageName")
                val appString = appLabel ?: packageName
                val appName = mapOf("app_added" to appString)
                val eventManager = EventManager(context)
                eventManager.sendGardionEvent(EventManager.GardionEventType.PACKAGE_ADDED, appName)
            }
        }
    }
}