package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.utils.EventManager

class PowerDisconnectedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d("GARDION_RECEIVER", "${this::class.java.simpleName}: broadcast ACTION_POWER_DISCONNECTED")
        if (intent?.action == Intent.ACTION_POWER_DISCONNECTED) {
            Log.d("GARDION_RECEIVER", "power disconnected")
            val eventManager = EventManager(context)
            eventManager.sendGardionEvent(EventManager.GardionEventType.POWER_DISCONNECTED)
        }
    }
}