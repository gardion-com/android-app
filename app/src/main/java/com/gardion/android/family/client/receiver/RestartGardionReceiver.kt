package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.ui.GardionVpnActivity
import com.gardion.android.family.client.utils.ReceiverUtils

class RestartGardionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d("GARDION_CONNECTION", "receiver ${javaClass.simpleName}")
        ReceiverUtils.connectVpnOnReceive(context, GardionVpnActivity.KEY_IS_FROM_DEACTIVATED_VPN)
    }
}