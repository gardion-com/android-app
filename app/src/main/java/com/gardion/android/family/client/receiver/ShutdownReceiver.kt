package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.logic.GardionApplication.Companion.usageData


class ShutdownReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_SHUTDOWN) {
            Log.i("SHUTDOWN", "Device will be shutdown, timestamp written to usageData")
            flowData.safeModeUsedChecked(false)
            usageData.saveDeviceLastShutdown(System.currentTimeMillis())
        }
    }
}