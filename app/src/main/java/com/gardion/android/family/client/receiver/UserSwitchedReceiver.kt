package com.gardion.android.family.client.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.utils.EventManager

class UserSwitchedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d("GARDION_RECEIVER", "${this::class.java.simpleName}: broadcast ACTION_USER_BACKGROUND")
        val eventManager = EventManager(context)
        eventManager.sendGardionEvent(EventManager.GardionEventType.USER_SWITCHED)
    }
}