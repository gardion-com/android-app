package com.gardion.android.family.client.security

import android.app.Service
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import com.gardion.android.family.client.ui.PopupBlockActivity
import com.gardion.android.family.client.utils.NotificationManager
import com.gardion.android.family.client.utils.Utils
import java.util.concurrent.TimeUnit

class CheckAdminService : Service() {

    private val binder: IBinder = EnableAdminLocalBinder()

    private lateinit var manager: DevicePolicyManager
    private lateinit var disposable: Disposable

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        manager = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        disposable = Observable.interval(3000L, TimeUnit.MILLISECONDS)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { showEnableAdminScreen(this) }
        toForegroundWithNotification()
        return START_STICKY
    }

    private fun showEnableAdminScreen(context: Context) {
        if (Utils.isDeviceAdminActive(context)) {
            stopSelf()
        } else {
            val launchActivity = Intent(this, PopupBlockActivity::class.java)
            launchActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(launchActivity)
        }
    }

    private fun toForegroundWithNotification() {
        val gardionNotificationManager = NotificationManager(this)
        startForeground(gardionNotificationManager.getNotificationId(NotificationManager.GardionNotificationType.DEVICE_LOCKED),
                gardionNotificationManager.createNotification(NotificationManager.GardionNotificationType.DEVICE_LOCKED, null, null))
    }

    override fun onBind(bindIntent: Intent?): IBinder {
        return binder
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }

    class EnableAdminLocalBinder : Binder() {
        fun getService(): CheckAdminService {
            return CheckAdminService()
        }
    }
}