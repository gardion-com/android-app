package com.gardion.android.family.client.security

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gardion.android.family.client.utils.ReceiverUtils
import com.gardion.android.family.client.ui.GardionVpnActivity

// reconnects VPN (if necessary)
class ReconnectVpnWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    override fun doWork(): Result {
        ReceiverUtils.connectVpnOnReceive(applicationContext, GardionVpnActivity.KEY_IS_FROM_DEACTIVATED_VPN)
        return Result.success()
    }

}