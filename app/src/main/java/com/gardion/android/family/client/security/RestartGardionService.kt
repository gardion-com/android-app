package com.gardion.android.family.client.security

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.ui.GardionVpnActivity
import com.gardion.android.family.client.receiver.RestartGardionReceiver
import java.lang.Exception

class RestartGardionService : Service() {

    companion object {
        var handler: Handler = Handler()
        lateinit var runnable: Runnable
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("GARDION_RESTART", "Gardion restart service")
        runnable = object : Runnable {
            override fun run() {
                restartApp()
                handler.postDelayed(this, 5000)
            }
        }
        handler.post(runnable)
        return START_STICKY
    }

    private fun restartApp() {
        if (!flowData.isVpnActivityOpen()!!) {
            val intent = Intent(this, GardionVpnActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!flowData.isGardionDeactivatedAllowed()!!) {
            val broadcastIntent = Intent(applicationContext, RestartGardionReceiver::class.java)
            sendBroadcast(broadcastIntent)
        } else {
            stopSelf()
            try {
                handler.removeCallbacks(runnable)
            } catch (e: Exception) {
                Log.e("GARDION_CONNECTION", e.toString())
            }
        }
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    class ConnectionLocalBinder : Binder() {
        fun getService(): RestartGardionService {
            return RestartGardionService()
        }
    }
}
