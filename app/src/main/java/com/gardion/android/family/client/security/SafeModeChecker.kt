package com.gardion.android.family.client.security

import android.app.usage.UsageEvents
import android.content.Context
import android.util.Log
import com.gardion.android.family.client.logic.GardionApplication
import com.gardion.android.family.client.logic.GardionApplication.Companion.usageData
import com.gardion.android.family.client.logic.UsageStats
import java.util.*


class SafeModeChecker {

    companion object {
        const val LOG_TAG = "SAFE_MODE"
    }


    // check if safe mode was used after the last registered shutdown
    // NOTE: experimental!
    fun safeModeUsed(context: Context): Boolean {
        val events = eventsAfterLastShutdown(context)
        return if (events != null) {
            val timeFirstEvent = timeFirstEventAfterShutdown(events)
            !wasGardionStarted(eventsCheck(context, timeFirstEvent))
        } else {
            Log.d(LOG_TAG, "no events found")
            false
        }
    }

    private fun eventsAfterLastShutdown(context: Context): UsageEvents? {
        val lastShutdown = usageData.getDeviceLastShutdown()
        return if (lastShutdown != null && lastShutdown != (-1).toLong()) {
            val startTime = lastShutdown + 200
            val endTime = System.currentTimeMillis()
            Log.d(LOG_TAG, "last shutdown: ${Date(startTime)}")
            Log.d(LOG_TAG, "end time: ${Date(endTime)}")
            UsageStats.getEvents(context, startTime, endTime)
        } else {
            null
        }
    }

    private fun timeFirstEventAfterShutdown(events: UsageEvents): Long {
        val firstEvent = UsageEvents.Event()
        events.getNextEvent(firstEvent)
        return firstEvent.timeStamp
    }

    private fun wasGardionStarted(events: UsageEvents): Boolean {
        var gardionStarted = false
        while (events.hasNextEvent() && !gardionStarted) {
            val event = UsageEvents.Event()
            events.getNextEvent(event)
            // TODO - add non-deprecated value for eventType
            if (event.packageName == GardionApplication.applicationContext().packageName &&
                    event.eventType == UsageStats.event_foreground) {
                Log.d(LOG_TAG, "Gardion was started after last boot")
                gardionStarted = true
            }
        }
        return gardionStarted
    }

    private fun eventsCheck(context: Context, timeFirstEvent: Long): UsageEvents {
        return UsageStats.getEvents(context, timeFirstEvent,
                timeFirstEvent + 2 * 60 * 1000)
    }

    // legacy from testing
    private fun logEvents(events: UsageEvents) {
        while (events.hasNextEvent()) {
            val event = UsageEvents.Event()
            events.getNextEvent(event)
            val output = event.packageName + ": " + event.eventType + " | " +
                    Date(event.timeStamp)
            Log.d(LOG_TAG, output)
        }
    }
}