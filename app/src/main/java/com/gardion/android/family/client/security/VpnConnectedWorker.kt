package com.gardion.android.family.client.security

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.gardion.android.family.client.utils.EventManager
import com.gardion.android.family.client.utils.EventManager.GardionEventType
import com.gardion.android.family.client.utils.Utils
import com.google.gson.JsonObject

// send out event to backend weather or not VPN is connected
class VpnConnectedWorker(context: Context, workerParams: WorkerParameters) :
        Worker(context, workerParams) {
    override fun doWork(): Result {
        val statusVpn = JsonObject()
        statusVpn.addProperty("is_vpn_connected",
                Utils.isVpnConnected(applicationContext))
        val eventManager = EventManager(applicationContext)
        val sendSuccess = eventManager.sendEventForWorker(GardionEventType.VPN_INTERVAL_CHECK,
                statusVpn)
        if (sendSuccess) {
            Result.success()
        } else {
            Result.retry()
        }
        return Result.success()
    }
}