package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.security.KeyChain
import android.security.KeyChainAliasCallback
import android.util.Base64
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.utils.NotificationManager
import kotlinx.android.synthetic.main.activity_gardion_certificate.*
import kotlinx.coroutines.*
import org.strongswan.android.data.VpnProfileDataSource
import kotlin.coroutines.CoroutineContext


class CertificateActivity : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    private val installPkcs12 = 100
    private val logTag = this::class.java.name

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, CertificateActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setContentView(R.layout.activity_gardion_certificate)
        install_certificate_button.setOnClickListener { installCertificate() }
    }

    private fun installCertificate() {
        val pkcs12Base64 = configData.getConfigurationPkcs12()
        val alias = configData.getConfigurationUserCertificateAlias() ?: "Gardion User"
        Log.d(logTag, pkcs12Base64.toString())
        if (pkcs12Base64 != null) {
            launch(Dispatchers.Default) {
                importPkcs12(pkcs12Base64, alias)
            }
        } else {
            Log.e(logTag, "installCertificate: no PKCS12 found in configData")
        }
    }

    private fun chooseKeyForVpn() {
        try {
            val database = VpnProfileDataSource(this)
            database.open()
            val vpnProfile = database.allVpnProfiles[0]
            database.close()
            Log.d(logTag, "userCertificateAlias: ${vpnProfile.userCertificateAlias}")
            if (vpnProfile.vpnType.toString() == "IKEV2_EAP_TLS") {
                chooseKey(vpnProfile.userCertificateAlias)
            }
        } catch (e: Exception) {
            Log.d(logTag, e.toString())
        }
    }

    private fun chooseKey(defaultAlias: String) {
        val keyChainAliasCallback = KeyChainAliasCallback {
            if (it != null) {
                Log.d(logTag, "installed Certificate: $it")
                val notificationManager = NotificationManager(this)
                notificationManager.cancelNotification(NotificationManager.GardionNotificationType.CERTIFICATE_REMOVED)
                finishActivity()
            } else {
                install_certificate_button.isEnabled = true
                Log.d(logTag, "not installed")
            }
            certificate_wait_text.visibility = GONE
        }
        KeyChain.choosePrivateKeyAlias(this, keyChainAliasCallback, null, null, null, -1, defaultAlias)
    }

    private fun importPkcs12(pkcs12Base64: String, alias: String) {
        try {
            val pkcs12ByteArray = Base64.decode(pkcs12Base64, Base64.DEFAULT)
            val intent = KeyChain.createInstallIntent()
            intent.putExtra(KeyChain.EXTRA_NAME, alias)
            intent.putExtra(KeyChain.EXTRA_PKCS12, pkcs12ByteArray)
            startActivityForResult(intent, installPkcs12)
        } catch (e: Exception) {
            Log.d(logTag, e.toString())
        }
    }

    private fun finishActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == installPkcs12) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(logTag, "installed certificate")
                install_certificate_button.isEnabled = false
                certificate_wait_text.visibility = VISIBLE
                chooseKeyForVpn()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d(logTag, "failed installing credentials")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}