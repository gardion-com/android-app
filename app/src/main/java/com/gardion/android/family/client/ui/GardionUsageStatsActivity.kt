package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import com.gardion.android.family.client.network.Links
import kotlinx.android.synthetic.main.activity_gardion_usage_stats.*


class GardionUsageStatsActivity : AppCompatActivity() {

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, GardionUsageStatsActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_usage_stats)
        usage_stats_start_button.setOnClickListener { startUsageStats() }
        contact_support_button.setOnClickListener { Links(this).goToForum() }
    }

    private fun startUsageStats() {
        finish()
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}
