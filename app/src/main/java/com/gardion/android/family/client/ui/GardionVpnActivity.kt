package com.gardion.android.family.client.ui

import android.app.Activity
import android.app.Service
import android.content.*
import android.net.Uri
import android.net.VpnService
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.FlowController
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.logic.UsageService
import com.gardion.android.family.client.logic.GardionVpnService
import com.gardion.android.family.client.network.Links
import com.gardion.android.family.client.network.Network
import com.gardion.android.family.client.security.RestartGardionService
import com.gardion.android.family.client.security.SafeModeChecker
import com.gardion.android.family.client.toast
import com.gardion.android.family.client.utils.*
import kotlinx.android.synthetic.main.activity_gardion_vpn.*
import kotlinx.coroutines.*
import org.strongswan.android.logic.VpnStateService
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext


class GardionVpnActivity : AppCompatActivity(), VpnStateService.VpnStateListener,
        PasswordDialog.GardionPasswordDialogListener, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var job: Job
    private var vpnStateService: VpnStateService? = null

    companion object {
        // TODO - check if we really need to differentiate what the cause for starting is
        const val KEY_IS_FROM_BOOT_RECEIVER = "key_is_from_boot_receiver"
        const val KEY_IS_FROM_USER_PRESENT_RECEIVER = "key_is_from_user_present"
        const val KEY_IS_FROM_NETWORK_AVAILABLE = "key_is_from_network_available"
        const val KEY_IS_FROM_DEACTIVATED_VPN = "key_is_from_deactivated_vpn"

        private const val PREPARE_VPN_REQUEST = 0

        private const val LOG_TAG = "GARDION_VPN"

        fun getIntent(activity: Activity): Intent {
            return Intent(activity, GardionVpnActivity::class.java)
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            vpnStateService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            vpnStateService = (service as VpnStateService.LocalBinder).service
            vpnStateService?.registerListener(this@GardionVpnActivity)
            stateChanged()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setContentView(R.layout.activity_gardion_vpn)
        applicationContext.bindService(Intent(applicationContext, VpnStateService::class.java),
                serviceConnection, Service.BIND_AUTO_CREATE)
        ControlAlarms.setResetDevicePickupsAlarm(this)
        initButtons()

        // TODO - check later if this can be simplified
        // atm we are checking on boot completed but also one time after each boot if not received
        if (intent != null && intent.extras != null) {
            if (intent?.extras?.getBoolean(KEY_IS_FROM_BOOT_RECEIVER, false)!!) {
                Log.d(LOG_TAG, "KEY_IS_FROM_BOOT_RECEIVER")
                checkSafeMode(this)
            }
        } else {
            if (flowData.isSafeModeUsedChecked() == null)
                checkSafeMode(this)
            else {
                if (!flowData.isSafeModeUsedChecked()!!) {
                    checkSafeMode(this)
                }
            }
        }

        val toolbar = findViewById<Toolbar>(R.id.gardion_toolbar)
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()

        if (!Utils.hasUsageStatsPermission(this)) {
            val intent = Intent(this, FlowController::class.java)
            startActivity(intent)
        } else {
            flowData.vpnActivityOpen(true)
            // TODO - maybe move this elsewhere? check with LG G4
            initButtons()
            if (!(vpnStateService?.state == VpnStateService.State.CONNECTED ||
                            vpnStateService?.state == VpnStateService.State.CONNECTING) &&
                    !Utils.isVpnConnected(this) &&
                    !flowData.isGardionDeactivatedAllowed()!!) {
                if (Utils.isInternetReady(this)) {
                    establishVpn(this)
                } else {
                    Network.requestNetworkInternet(this)
                    // TODO - remove if really not needed any more
//                    if (intent != null && intent.extras != null &&
//                            intent?.extras?.getBoolean(KEY_IS_FROM_BOOT_RECEIVER, false)!!) {
//                        Log.d(LOG_TAG, "KEY_IS_FROM_BOOT_RECEIVER")
//                        Network.requestNetworkInternet(this)
//                    } else {
//                        Log.i(LOG_TAG, "no network available")
//                        toast(getString(R.string.general_toast_device_offline))
//                    }
                }
            }
        }
    }

    private fun initButtons() {
        Log.i(LOG_TAG, "initialize buttons...")
        vpn_status_reconnect_button.setOnClickListener { reconnectVpn(this) }
        vpn_status_disconnect_button.setOnClickListener { tryDisconnectVpn(this) }
        vpn_reset_gardion_button.setOnClickListener { startGardionResetActivity(this) }
        contact_support_button.setOnClickListener { Links(this).goToForum() }
//        test_1_button.setOnClickListener { testWorker(this) }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.action_privacy_policy -> {
                val intent = Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.url_privacy_policy)))
                startActivity(intent)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        flowData.vpnActivityOpen(false)
        // TODO - activate later again?
//        if (!flowData.isGardionDeactivatedAllowed()!! && !Utils.isVpnConnected(this)) {
//            Log.d(LOG_TAG, "starting startRestartService...")
//            startRestartService()
//        }
    }

    override fun onDestroy() {
        if (flowData.isTamperingModeActive() == true) {
            Log.d(LOG_TAG, "tampering, startGardionRestartWorker")
            ControlWorkers.startGardionRestartWorker(this)
        }
        job.cancel()
        super.onDestroy()
    }

    private fun establishVpn(context: Context) {
        Log.i(LOG_TAG, "establish Vpn...")

        if (Utils.isInternetReady(context)) {
            launch {
                try {
                    val eventManager = EventManager(context)
                    val existsDevice = suspend { eventManager.isDeviceInBackend() }
                    if (existsDevice.invoke()) {
                        withContext(Dispatchers.Main) { onDeviceFound(context) }
                    } else {
                        withContext(Dispatchers.Main) { onDeviceNotFound() }
                    }
                } catch (e: SocketTimeoutException) {
                    onConnectionFailed(e)
                } catch (e: ConnectException) {
                    onConnectionFailed(e)
                } catch (e: UnknownHostException) {
                    onConnectionFailed(e)
                }
            }
        } else {
            Log.i(LOG_TAG, "no network available")
            toast(getString(R.string.general_toast_device_offline))
        }
    }

    private fun prepareVpn(context: Context) {
        Log.i(LOG_TAG, "prepare Vpn...")
        val intent = VpnService.prepare(context)
        // returns null if VPN is already prepared:
        if (intent != null) {
            startActivityForResult(intent, PREPARE_VPN_REQUEST)
        } else {
            onActivityResult(PREPARE_VPN_REQUEST, Activity.RESULT_OK, null)
        }
    }

    // VPN must be prepared before starting with 'prepareVpn'
    private fun startVpnService(context: Context) {
        Log.i(LOG_TAG, "start VPN...")

        launch {
            val intent = Intent(context, GardionVpnService::class.java)
            // TODO - check if we should use startForegroundService only after rebooting the device; add explanation
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                /**
                 * with this Android version we need to start service in foreground when app is
                 * still in background (e.g. can happen after a reboot of the device
                 */
                startForegroundService(intent)
            } else {
                startService(intent)
            }
        }
    }

    private fun reconnectVpn(context: Context) {
        vpn_status_reconnect_button.isActivated = false
        disableReconnectButtonTemporary()
        // switching flight mode off sometimes leads to a wrong result of isInternetReady -> wait
//        Thread.sleep(1000)
        if (Utils.isInternetReady(context)) {
            Log.i(LOG_TAG, "Reconnecting...")
            disconnectVpn(context)
            establishVpn(context)
        } else {
            toast(getString(R.string.general_toast_device_offline))
            Log.i(LOG_TAG, "no network available")
        }
    }

    private fun disconnectVpn(context: Context) {
        launch {
            val intent = Intent(context, GardionVpnService::class.java)
            intent.action = GardionVpnService.DISCONNECT_ACTION
            context.startService(intent)
        }
    }

    override fun stateChanged() {
        Log.i(LOG_TAG, "stateChanged, state: ${vpnStateService?.state}")
        val state = vpnStateService?.state
        val errorState = vpnStateService?.errorState

        when (state) {
            VpnStateService.State.CONNECTING -> onConnecting()
            VpnStateService.State.CONNECTED -> onConnected()
            VpnStateService.State.DISCONNECTING -> onDisconnecting()
            VpnStateService.State.DISABLED -> onDisabled()
        }

        if (errorState != VpnStateService.ErrorState.NO_ERROR) {
            onError(errorState)
        }
    }

    private fun onConnecting() {
        vpn_status_info.text = getString(R.string.vpn_status_connecting)
        vpn_status_image.setImageResource(R.drawable.ic_conn_sync)
    }

    private fun onConnected() {
        vpn_status_info.text = getString(R.string.vpn_status_connected)
        vpn_status_image.setImageResource(R.drawable.ic_conn_success)
        enableDisconnectButton()
        val eventManager = EventManager(this)
        eventManager.sendGardionEvent(EventManager.GardionEventType.VPN_CONNECTED)
        val gardionNotificationManager = NotificationManager(this)
        gardionNotificationManager.cancelNotification(NotificationManager.GardionNotificationType.GARDION_DEACTIVATED)
        gardionNotificationManager.cancelNotification(NotificationManager.GardionNotificationType.START_VPN_FOREGROUND)
        startGardionServices(this)
        flowData.gardionDeactivatedAllowed(false)
        flowData.gardionSetupSuccessful(true)
        stopRestartService(this)
        flowData.tamperingModeActive(false)
        ControlWorkers.stopGardionRestartWorker(this)
    }

    private fun onDisconnecting() {
        vpn_status_info.text = getString(R.string.vpn_status_disconnecting)
        vpn_status_image.setImageResource(R.drawable.ic_conn_sync)
    }

    private fun onDisabled() {
        val eventManager = EventManager(this)
        eventManager.sendGardionEvent(EventManager.GardionEventType.VPN_DISCONNECTED)
        if (flowData.isGardionSetupSuccessful()!!) {
            if (flowData.isGardionDeactivatedAllowed()!!) {
                onDisabledAllowed()
            } else {
                onDisabledForbidden()
            }
        } else {
            vpn_status_info.text = getString(R.string.vpn_status_disconnected)
            vpn_status_image.setImageResource(R.drawable.ic_conn_fail)
        }
    }

    private fun onDisabledAllowed() {
        vpn_status_info.text = getString(R.string.vpn_status_disabled)
        vpn_status_image.setImageResource(R.drawable.ic_conn_fail)
        stopRestartService(this)
//        enableResetButton()
        stopGardionServices(this)
    }

    private suspend fun onConnectionFailed(e: Exception) {
        Log.e(LOG_TAG, "exception: $e")
        withContext(Dispatchers.Main) {
            vpn_status_info.text = getString(R.string.vpn_status_noserver)
            vpn_status_image.setImageResource(R.drawable.ic_conn_fail)
            toast(getString(R.string.toast_no_connection_possible))
        }
    }

    private fun onDisabledForbidden() {
        // TODO - check what we really need here
//        establishVpn(this)
    }

    private fun onError(errorState: VpnStateService.ErrorState?) {
        Log.e(LOG_TAG, "errorState:  $errorState")
        if (errorState == VpnStateService.ErrorState.GENERIC_ERROR) {
            launch {
                delay(10000L)
                establishVpn(this@GardionVpnActivity)
            }
        }
    }

    private fun onDeviceFound(context: Context) {
        Log.i(LOG_TAG, "Device found")
        prepareVpn(context)
    }

    private fun onDeviceNotFound() {
        Log.i(LOG_TAG, "Device not found")
        flowData.gardionSetupSuccessful(false)
        flowData.gardionDeactivatedAllowed(true)
        vpn_status_info.text = getString(R.string.vpn_status_disabled_no_device)
        vpn_status_image.setImageResource(R.drawable.ic_conn_fail)
        toast(getString(R.string.toast_device_not_exist_in_backend))
        enableResetButton()
    }

    private fun tryDisconnectVpn(context: Context) {
        if (configData.getConfigurationUseParentPin()!!) {
            showDialogWithPasswordInput()
        } else {
            flowData.gardionDeactivatedAllowed(true)
            onDeactivated(context)
        }
    }

    private fun showDialogWithPasswordInput() {
        val gardionDialog = PasswordDialog()
        gardionDialog.show(supportFragmentManager, "fragment_gardion_dialog")
    }

    private fun onDeactivated(context: Context) {
        disconnectVpn(context)
        enableResetButton()
        toast(getString(R.string.password_toast_gardion_deactivated))
        val eventManager = EventManager(this)
        eventManager.sendGardionEvent(EventManager.GardionEventType.VPN_DEACTIVATED)
        val notificationManager = NotificationManager(this)
        notificationManager.notify(
                NotificationManager.GardionNotificationType.GARDION_DEACTIVATED,
                null,
                null)
    }

    // action after user has entered parent PIN
    override fun onFinishEditDialog(inputText: String) {
        val hashedInputText = Utils.hashSha256(inputText)
        val savedParentPin = configData.getConfigurationParentPin()
        if (hashedInputText == savedParentPin) {
            flowData.gardionDeactivatedAllowed(true)
            onDeactivated(this)
        } else {
            toast(getString(R.string.password_toast_pin_wrong))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PREPARE_VPN_REQUEST) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    startVpnService(this)
                }
                Activity.RESULT_CANCELED -> {
                    if (!flowData.isGardionDeactivatedAllowed()!!) {
                        toast(getString(R.string.vpn_reallow_vpn))
                        // establishVpn is called automatically here via onResume
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun enableResetButton() {
        vpn_status_disconnect_button.visibility = View.GONE
        vpn_reset_gardion_button.visibility = View.VISIBLE
    }

    private fun enableDisconnectButton() {
        vpn_status_disconnect_button.visibility = View.VISIBLE
        vpn_reset_gardion_button.visibility = View.GONE
    }

    private fun disableReconnectButtonTemporary() {
        vpn_status_reconnect_button.isEnabled = false
        vpn_status_reconnect_button.postDelayed({ vpn_status_reconnect_button.isEnabled = true }, 3000)
    }

    private fun startGardionResetActivity(context: Context) {
        val intent = Intent(context, ResetGardionActivity::class.java)
        startActivity(intent)
    }

    private fun startGardionServices(context: Context) {
        startService(Intent(context, UsageService::class.java))
    }

    private fun stopGardionServices(context: Context) {
        stopService(Intent(context, UsageService::class.java))
    }

    // leaving this here for later
    private fun startRestartService() {
        try {
            if (!Utils.isMyServiceRunning(RestartGardionService::class.java, this)) {
                val intent = Intent(applicationContext, RestartGardionService::class.java)
                startService(intent)
            }
        } catch (e: Exception) {
            Log.e(LOG_TAG, e.toString())
        }
    }

    private fun stopRestartService(context: Context) {
        Log.d(LOG_TAG, "stopRestartService")
        val intent = Intent(context, RestartGardionService::class.java)
        stopService(intent)
    }


    // check if device was in safe mode before (experimental)
    private fun checkSafeMode(context: Context) {
        Log.d(LOG_TAG, "checking if safe mode was used...")
        val checker = SafeModeChecker()
        if (checker.safeModeUsed(context)) {
            val manager = EventManager(context)
            manager.sendGardionEvent(EventManager.GardionEventType.SAFE_MODE_USED)
            Log.d(LOG_TAG, "safe mode used")
        }
        flowData.safeModeUsedChecked(true)
    }
}