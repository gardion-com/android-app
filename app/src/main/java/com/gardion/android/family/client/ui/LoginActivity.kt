package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_gardion_login.*
import com.gardion.android.family.client.R
import org.strongswan.android.data.VpnProfile
import org.strongswan.android.data.VpnProfileDataSource
import org.strongswan.android.data.VpnType
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.network.GardionApi
import com.gardion.android.family.client.network.Links
import com.gardion.android.family.client.network.model.GardionData
import com.gardion.android.family.client.toast
import com.gardion.android.family.client.utils.Utils
import kotlinx.coroutines.*
import java.net.SocketTimeoutException
import java.util.*
import kotlin.coroutines.CoroutineContext


class LoginActivity : AppCompatActivity(), TextWatcher, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private lateinit var job: Job

    companion object {
        private const val LOG_TAG = "GARDION_LOGIN"
        const val INTENT_CERTIFICATE_USED: String = "intent_certificate_used"
        const val INTENT_PARENT_PIN: String = "intent_parent_pin"

        fun getIntent(activity: Activity): Intent {
            return Intent(activity, LoginActivity::class.java)
        }
    }

    private val api: GardionApi = GardionApi.instance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setContentView(R.layout.activity_gardion_login)
        gardion_login_pincode.addTextChangedListener(this)
        login_button.setOnClickListener { buttonLogin() }
        contact_support_button.setOnClickListener { Links(this).goToForum() }
    }

    private fun buttonLogin() {
        val gardionCode = gardion_login_pincode.text.toString()
        when {
            !Utils.isGardionCodeValid(gardionCode) -> toast(getString(R.string.login_toast_too_short))
            !Utils.isInternetReady(this) -> toast(getString(R.string.general_toast_device_offline))
            else -> fetchData(gardionCode)
        }
        Utils.hideKeyboard(this)
    }

    private fun fetchData(gardionCode: String) {
        login_button.isEnabled = false

        launch {
            var returnMessage = ""
            try {
                val response = suspend {
                    api.fetchGardionData(gardionCode).execute()
                }.invoke()
                if (response.isSuccessful) {
                    val gardionProfile = response.body()!!
                    saveToDataBase(gardionProfile)
                    returnMessage = getString(R.string.login_toast_success)
                    finishWithData(Activity.RESULT_OK, gardionProfile.connection.authentication.authType, gardionProfile.device.parentPin)
                } else {
                    val responseCode = response.code()
                    returnMessage = when (responseCode) {
                        // if gardionCode does not exist at backend response code 500 is received
                        // code 500 can of course also result from different events on server
                        // server should actually send a message / code stating explicitly that the gardionCode does not exist
                        500 -> "${getString(R.string.login_toast_error_500)} (Code: $responseCode)"
                        401 -> "${getString(R.string.login_toast_error_401)} (Code: $responseCode)"
                        else -> "${getString(R.string.login_toast_error_general)} (Code: $responseCode)"
                    }
                    Log.w(LOG_TAG, "Could not fetch Gardion profile, response code: $responseCode")
                }
            } catch (e: SocketTimeoutException) {
                Log.e(LOG_TAG, "TIMEOUT: ${e.message}")
                withContext(Dispatchers.Main) {
                    returnMessage = getString(R.string.toast_no_connection_possible)
                }
            } catch (e: Exception) {
                Log.w(LOG_TAG, "${e.message}")
                withContext(Dispatchers.Main) {
                    returnMessage = getString(R.string.toast_unknown_error) + " (Fehler: ${e.message})"
                }

            } finally {
                withContext(Dispatchers.Main) {
                    login_button.isEnabled = true
                    toast(returnMessage)
                }
            }
        }

    }

    private fun finishWithData(result: Int, vpnType: String, parentPin: String) {
        val data = Intent()

        if (vpnType == "IKEV2_EAP_TLS") {
            data.putExtra(INTENT_CERTIFICATE_USED, true)
        } else {
            data.putExtra(INTENT_CERTIFICATE_USED, false)
        }
        data.putExtra(INTENT_PARENT_PIN, parentPin)
        setResult(result, data)
        finish()
    }

    private fun saveToDataBase(gardionData: GardionData) {
        val gardionNameVpn = gardionData.connection.name
        val gardionUrl = gardionData.connection.url[0]
        val gardionUsername = gardionData.connection.authentication.user_id
        val gardionPassword = gardionData.connection.authentication.password
        val gardionVpnType = gardionData.connection.authentication.authType
        val gardionUserCertificateAlias = gardionData.connection.authentication.userCertificateAlias
        val gardionExcludedApps = gardionData.connection.excludedApps?.toSortedSet()
        val gardionExcludedSubnets = gardionData.connection.localRouting

        val vpnProfile = VpnProfile()
        vpnProfile.name = gardionNameVpn
        vpnProfile.gateway = gardionUrl
        vpnProfile.username = gardionUsername
        vpnProfile.password = gardionPassword
        vpnProfile.uuid = UUID.randomUUID()
        vpnProfile.vpnType = VpnType.valueOf(gardionVpnType)
        vpnProfile.userCertificateAlias = gardionUserCertificateAlias
        vpnProfile.excludedSubnets = gardionExcludedSubnets?.joinToString(" ")

        if (gardionExcludedApps != null) {
            if (gardionExcludedApps.isNotEmpty()) {
                vpnProfile.setSelectedApps(gardionExcludedApps)
                vpnProfile.selectedAppsHandling = VpnProfile.SelectedAppsHandling.SELECTED_APPS_EXCLUDE
            }
        }

        val database = VpnProfileDataSource(this)
        database.open()
        database.insertProfile(vpnProfile)
        database.close()
        flowData.setVpnProfileSaved(true)

        saveConfigurationDataToSharedPrefs(gardionData)
    }

    private fun saveConfigurationDataToSharedPrefs(data: GardionData) {
        configData.saveConfigurationDeviceId(data.device.id)
        configData.saveConfigurationDeviceName(data.device.name)
        configData.saveConfigurationGroupId(data.group.id)
        configData.saveConfigurationGroupName(data.group.name)
        configData.saveConfigurationPkcs12(data.connection.authentication.pkcs12Base64)
        configData.saveConfigurationUserCertificateAlias(data.connection.authentication.userCertificateAlias)
        val hashedParentPin = Utils.hashSha256(data.device.parentPin)
        configData.saveConfigurationParentPin(hashedParentPin)
        configData.saveConfigurationVpnName(data.connection.name)
        configData.saveConfigurationExcludedApps(data.connection.excludedApps)
        configData.saveConfigurationExcludedSubnets(data.connection.localRouting)
        configData.saveConfigurationUseParentPin(data.device.useParentPin)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        login_button.isEnabled = Utils.isGardionCodeValid(gardion_login_pincode.text.toString())
    }
}