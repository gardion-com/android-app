package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gardion.android.family.client.R
import kotlinx.android.synthetic.main.activity_gardion_system_alert_window.*
import android.os.Build
import androidx.annotation.RequiresApi


class OverlayPermissionActivity : AppCompatActivity() {

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, OverlayPermissionActivity::class.java)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_system_alert_window)

        system_alert_window_start_button.setOnClickListener { startSystemAlertWindow() }
    }

    private fun startSystemAlertWindow() {
        finish()
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}
