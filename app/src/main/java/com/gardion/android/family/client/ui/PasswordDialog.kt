package com.gardion.android.family.client.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.gardion_dialog_password_fragment.*
import com.gardion.android.family.client.R
import com.gardion.android.family.client.utils.Utils


class PasswordDialog : DialogFragment(), TextWatcher {

    interface GardionPasswordDialogListener {
        fun onFinishEditDialog(inputText: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.gardion_dialog_password_fragment, container)
    }

    override fun onStart() {
        super.onStart()
        Utils.forceKeyboardOpenFragment(activity)
        password_dialog_editText.requestFocus()
        password_dialog_editText.addTextChangedListener(this)
        password_dialog_unlock_button.setOnClickListener { takeTypedValueAndDismissDialog() }
        password_dialog_cancel.setOnClickListener { cancelGoBack() }
    }

    private fun takeTypedValueAndDismissDialog() {
        val listener: GardionPasswordDialogListener = activity as GardionPasswordDialogListener
        listener.onFinishEditDialog(password_dialog_editText.text.toString())
        this.dismiss()
    }

    private fun cancelGoBack() {
        this.dismiss()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        password_dialog_unlock_button.isEnabled = Utils.isGardionCodeValid(password_dialog_editText.text.toString())
    }
}