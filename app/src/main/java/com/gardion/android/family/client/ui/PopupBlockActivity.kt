package com.gardion.android.family.client.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_popup_gardion.*
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.FlowController
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.security.CheckAdminService
import com.gardion.android.family.client.toast
import com.gardion.android.family.client.utils.NotificationManager
import com.gardion.android.family.client.utils.Utils


class PopupBlockActivity : AppCompatActivity(), TextWatcher {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popup_gardion)
        enable_admin_go_to_settings_button.setOnClickListener { goToDeviceAdminSettings() }
        popup_ask_password_editText.addTextChangedListener(this)
        unlock_gardion_button.setOnClickListener { unlockGardion() }
        unlock_view_group.visibility = VISIBLE
    }

    override fun onStart() {
        super.onStart()
        //check if device admin is still not active
        if (Utils.isDeviceAdminActive(this)) {
            val intent = Intent(this, FlowController::class.java)
            startActivity(intent)
        }
        unlock_view_group.visibility = VISIBLE
        secured_content_group.visibility = GONE
    }

    override fun onStop() {
        super.onStop()
        popup_ask_password_editText.text.clear()
    }

    private fun unlockGardion() {
        when (configData.getConfigurationParentPin()) {
            Utils.hashSha256(popup_ask_password_editText.text.toString()) -> unlockTillLockScreen()
            else -> toast(getString(R.string.password_toast_pin_wrong))
        }
    }

    private fun unlockTillLockScreen() {
        stopService(Intent(this, CheckAdminService::class.java))
        flowData.setGardionUnlocked(true)
        toast(getString(R.string.popup_toast_unlocked))
        showSecuredContent()
        cancelNotificationDeviceLocked(this)
    }

    private fun showSecuredContent() {
        unlock_view_group.visibility = GONE
        secured_content_group.visibility = VISIBLE
        secure_uninstall_gardion_button.setOnClickListener { uninstallGardion() }
    }

    private fun uninstallGardion() {
        val uninstallIntent = Intent(Intent.ACTION_DELETE)
        uninstallIntent.data = Uri.parse("package:$packageName")
        finish()
        startActivity(uninstallIntent)
    }

    private fun goToDeviceAdminSettings() {
        startActivity(Intent().setComponent(ComponentName("com.android.settings", "com.android.settings.DeviceAdminSettings")))
    }

    private fun cancelNotificationDeviceLocked(context: Context) {
        val gardionNotificationManager = NotificationManager(context)
        gardionNotificationManager.cancelNotification(NotificationManager.GardionNotificationType.DEVICE_LOCKED)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        unlock_gardion_button.isEnabled = Utils.isGardionCodeValid(popup_ask_password_editText.text.toString())
    }
}