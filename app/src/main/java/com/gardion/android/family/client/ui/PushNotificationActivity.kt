package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gardion.android.family.client.R
import com.gardion.android.family.client.fragments.GardionSettingsPushFragment
import com.gardion.android.family.client.network.PushNotificationService
import kotlinx.android.synthetic.main.activity_gardion_push_notification.*


class PushNotificationActivity : AppCompatActivity() {

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, PushNotificationActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_push_notification)
        push_notifications_continue_button.setOnClickListener { finishPushNotificationPreference() }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings_push, GardionSettingsPushFragment())
                .commit()
    }

    private fun finishPushNotificationPreference() {
        PushNotificationService.sendTokenToServer(this)
        finish()
    }
}