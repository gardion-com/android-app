package com.gardion.android.family.client.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import com.gardion.android.family.client.utils.EventManager
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_gardion_report_url.*


class ReportUrl : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_report_url)

        if (intent.action == Intent.ACTION_SEND && "text/plain" == intent.type) {
            val url = handleReceiveText(intent)
            report_url_submit_button.setOnClickListener {
                shareUrlToGardion(
                        this,
                        url,
                        report_url_reason.text.toString())
            }
            report_url_back_button.setOnClickListener { finish() }
        }
    }

    private fun handleReceiveText(intent: Intent): String? {
        intent.getStringExtra(Intent.EXTRA_TEXT).let {
            report_url_url.text = it
            return it
        }
    }

    private fun shareUrlToGardion(context: Context, url: String?, reason: String?) {
        val payload = JsonObject()
        payload.addProperty("url_reported", url)
        payload.addProperty("url_reason", reason)

        val report = mapOf(
                "url_reported" to url,
                "url_reason" to reason
        )

        val eventManager = EventManager(context)
        eventManager.sendGardionEvent(EventManager.GardionEventType.REPORT_URL, report)
        urlSubmitted()
    }

    private fun urlSubmitted() {
        report_url_group.visibility = GONE
        report_url_submitted_group.visibility = VISIBLE
    }
}