package com.gardion.android.family.client.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.gardion.android.family.client.R
import com.gardion.android.family.client.utils.NotificationManager
import kotlinx.android.synthetic.main.activity_gardion_reset.*
import org.strongswan.android.data.VpnProfileDataSource
import android.app.ActivityManager
import android.app.admin.DevicePolicyManager
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.data.datasource.SharedPreferencesDataStore
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.receiver.GardionDeviceAdminReceiver


class ResetGardionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_reset)

        reset_gardion_data_button.setOnClickListener { resetGardionData(this) }
        reset_gardion_back_button.setOnClickListener { backToVpn(this) }
    }

    private fun resetGardionData(context: Context) {
        removeVpnProfiles()

        val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        dpm.removeActiveAdmin(GardionDeviceAdminReceiver.getComponentName(this))

        val sharedPrefs = this.getSharedPreferences(SharedPreferencesDataStore.PREFERENCES_NAME, Context.MODE_PRIVATE)
        sharedPrefs.edit().clear().apply()

        val notificationManager = NotificationManager(context)
        notificationManager.cancelAllNotifications()

        // TODO - an event should be send out to warn / tell the parent that the device was reset
        // TODO - app gets closed before event can be send out. see also https://gitlab.com/gardion-com/dev/android-app/issues/121
        //val eventManager =  EventManager(context)
        //eventManager.sendGardionEvent(EventManager.GardionEventType.GARDION_RESET)

        // TODO - disconnect existing VPN connection, its safer (although user can at the moment only reset Gardion after having deactivated it

        // this action should be the last because it causes the app to be closed
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.clearApplicationUserData()

    }

    private fun backToVpn(context: Context) {
        val intent = Intent(context, GardionVpnActivity::class.java)
        startActivity(intent)
    }

    private fun removeVpnProfiles() {
        val vpnDatabase = VpnProfileDataSource(this)
        vpnDatabase.open()
        val vpnProfiles = vpnDatabase.allVpnProfiles
        if (vpnProfiles.isNotEmpty()) {
            vpnProfiles.forEach {
                if (it.name == configData.getConfigurationVpnName()) {
                    vpnDatabase.deleteVpnProfile(it)
                }
            }
        }
        vpnDatabase.close()
    }
}