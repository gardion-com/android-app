package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import kotlinx.android.synthetic.main.activity_gardion_show_parent_pin.*


class ShowParentPinActivity : AppCompatActivity() {

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, ShowParentPinActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_show_parent_pin)
        val intent = intent
        val parentPin = intent.extras?.getString(LoginActivity.INTENT_PARENT_PIN)
        parent_pin_view.text = parentPin
        show_parent_pin_next_button.setOnClickListener { startDeviceAdmin() }
    }

    private fun startDeviceAdmin() {
        finish()
    }
}