package com.gardion.android.family.client.ui

import android.app.Activity
import android.os.Bundle
import android.security.KeyChain
import android.security.KeyChainAliasCallback
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import kotlinx.android.synthetic.main.activity_test_key_chain_changed.*
import kotlinx.coroutines.*
import java.lang.Exception
import java.security.KeyStore
import java.security.cert.X509Certificate
import javax.security.auth.x500.X500Principal
import kotlin.coroutines.CoroutineContext


class TestKeyChainChangedActivity : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job
    private lateinit var job: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setContentView(R.layout.activity_test_key_chain_changed)
        button_test_123.setOnClickListener { showAliases() }
    }

    private fun chooseKey(activity: Activity) {
        // TODO - issuer should not be hard coded but taken from Gardion profile
        val issuer = X500Principal("CN=Duke, OU=JavaSoft, O=Sun Microsystems, C=US")

        val issuers = arrayOf(issuer)

        val keyChainAliasCallback = KeyChainAliasCallback {
            Log.d("GARDION_CERTIFICATE", "callback $it")
        }

        try {
            launch {
                KeyChain.choosePrivateKeyAlias(activity, keyChainAliasCallback,
                        null, issuers, null, -1, "Gardion/Benni/Pixel")
            }
        } catch (e: Exception) {
            Log.d("GARDION_CERTIFICATE",
                    "certificate not found, error ${e.printStackTrace()}")
        }
    }

    private fun showAliases() {
        Log.d("GARDION_CERTIFICATE", "button pressed")

        val ks = KeyStore.getInstance("AndroidCAStore")
        ks.load(null, null)
        val containsAlias = ks.containsAlias("Gardion/Benni")

        val ksEntry = ks.getEntry("Gardion/Benni", null)

        Log.d("GARDION_CERTIFICATE", "contains alias: $containsAlias")
        Log.d("GARDION_CERTIFICATE", "ksEntry: $ksEntry")

        val aliases = ks.aliases()

        while (aliases.hasMoreElements()) {
            val alias = aliases.nextElement()
            Log.d("GARDION_CERTIFICATE", "----")
            Log.d("GARDION_CERTIFICATE", alias)
            val cert = ks.getCertificate(alias) as X509Certificate
            Log.d("GARDION_CERTIFICATE", "issuerDN: ${cert.issuerDN}")
            Log.d("GARDION_CERTIFICATE", "issuerDN: ${cert.subjectDN}")
        }
    }
}