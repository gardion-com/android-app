package com.gardion.android.family.client.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gardion.android.family.client.R
import com.gardion.android.family.client.toast
import com.gardion.android.family.client.utils.Utils
import kotlinx.android.synthetic.main.activity_gardion_welcome.*


class WelcomeActivity : AppCompatActivity() {

    companion object {
        fun getIntent(activity: Activity): Intent {
            return Intent(activity, WelcomeActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gardion_welcome)
        checkOnline()
        start_initial_setup_button.setOnClickListener { startInitialSetup() }
    }

    private fun startInitialSetup() {
        finish()
    }

    private fun checkOnline() {
        if (!Utils.isInternetReady(this)) toast(getString(R.string.general_toast_device_offline))
    }
}