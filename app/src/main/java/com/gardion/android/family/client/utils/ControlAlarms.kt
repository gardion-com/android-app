package com.gardion.android.family.client.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import com.gardion.android.family.client.logic.DevicePickupsResetService

class ControlAlarms {
    companion object {
        fun setResetDevicePickupsAlarm(context: Context) {

            Log.d("GARDION", "setResetDevicePickupsAlarm")

            val pendingIntent = PendingIntent.getService(
                    context,
                    0,
                    Intent(context, DevicePickupsResetService::class.java),
                    PendingIntent.FLAG_UPDATE_CURRENT)
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Utils.calEndOfToday().timeInMillis,
                    AlarmManager.INTERVAL_DAY, pendingIntent)
        }
    }
}