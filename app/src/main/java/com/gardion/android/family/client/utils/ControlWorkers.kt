package com.gardion.android.family.client.utils

import android.content.Context
import android.util.Log
import androidx.work.*
import com.gardion.android.family.client.logic.UsageStatsWorker
import com.gardion.android.family.client.network.EventWorker
import com.gardion.android.family.client.security.ReconnectVpnWorker
import com.gardion.android.family.client.security.VpnConnectedWorker
import java.util.concurrent.TimeUnit


class ControlWorkers {
    companion object {
        private const val LOG_TAG = "GARDION_WORKER"

        fun startGardionActiveWorkers(context: Context) {
            startVpnConnectedWorker(context)
            startReconnectVpnWorker(context)
        }

        fun stopGardionActiveWorkers(context: Context) {
            WorkManager.getInstance(context).cancelAllWorkByTag("gardion_active")
        }

        private fun startVpnConnectedWorker(context: Context) {
            val workRequest = PeriodicWorkRequestBuilder<VpnConnectedWorker>(
                    60, TimeUnit.MINUTES, 30, TimeUnit.MINUTES)
                    .setConstraints(constraintInternet())
                    .addTag("gardion_active")
                    .build()
            WorkManager.getInstance(context).enqueue(workRequest)
            Log.d(LOG_TAG, "started startVpnConnectedWorker")
        }

        private fun startReconnectVpnWorker(context: Context) {
            val workRequest = PeriodicWorkRequestBuilder<ReconnectVpnWorker>(
                    15, TimeUnit.MINUTES, 5, TimeUnit.MINUTES)
                    .setConstraints(constraintInternet())
                    .addTag("gardion_active")
                    .build()
            WorkManager.getInstance(context).enqueue(workRequest)
            Log.d(LOG_TAG, "started startReconnectVpnWorker")
        }

        fun startGardionRestartWorker(context: Context) {
            if (!Utils.isVpnConnected(context)) {
                val workRequest = OneTimeWorkRequestBuilder<ReconnectVpnWorker>()
                        .setConstraints(constraintInternet())
                        .addTag("gardion_reconnect")
                        .build()
                WorkManager.getInstance(context).enqueue(workRequest)
                Log.d(LOG_TAG, "started ReconnectWorker")
            } else {
                Log.d(LOG_TAG, "already connected to VPN")
            }
        }

        fun stopGardionRestartWorker(context: Context) {
            WorkManager.getInstance(context).cancelAllWorkByTag("gardion_reconnect")
        }

        fun startEventWorker(context: Context, eventId: String, payload: Map<String, String?>? = null) {
            val workRequest = OneTimeWorkRequestBuilder<EventWorker>()
                    .setConstraints(constraintInternet())
                    .setInputData(getEventData(eventId, payload))
                    .addTag("gardion_event")
                    .build()
            WorkManager.getInstance(context).enqueue(workRequest)
            Log.d(LOG_TAG, "started startEventWorker")
        }

        fun startUsageStatsWorker(context: Context) {
            val workRequest = OneTimeWorkRequestBuilder<UsageStatsWorker>()
                    .setConstraints(constraintInternet())
                    .addTag("usage_stats")
                    .build()
            WorkManager.getInstance(context).enqueueUniqueWork("usage_stats",
                    ExistingWorkPolicy.REPLACE, workRequest)
            Log.d(LOG_TAG, "started startUsageStatsWorker")
        }

        private fun getEventData(eventId: String, payload: Map<String, String?>? = null): Data {
            val data = Data.Builder()
            data.putString("event_id", eventId)
            payload?.forEach { (key, value) ->
                data.putString(key, value)
            }
            return data.build()
        }

        private fun constraintInternet(): Constraints {
            return Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
        }
    }
}