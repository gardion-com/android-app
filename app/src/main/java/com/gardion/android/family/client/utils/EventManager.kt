package com.gardion.android.family.client.utils

import android.content.Context
import android.util.Log
import com.gardion.android.family.client.R
import com.gardion.android.family.client.logic.GardionApplication.Companion.configData
import com.gardion.android.family.client.network.GardionApi
import com.gardion.android.family.client.network.model.GardionEvent
import com.google.gson.JsonObject


class EventManager(private val context: Context) {

    private val api: GardionApi = GardionApi.instance

    enum class GardionEventType {
        ADMIN_DEACTIVATION, VPN_DISCONNECTED, VPN_REMOVED, VPN_INTERVAL_CHECK,
        VPN_CONNECTED, VPN_DEACTIVATED, TEST_DEV, ADMIN_ACTIVATION, PACKAGE_ADDED, USAGE_STATS,
        POWER_DISCONNECTED, DEVICE_EXISTS_BACKEND, GARDION_RESET, CERTIFICATE_REMOVED,
        NO_USAGE_STATS_PERMISSION, REPORT_URL, PUSH_NOTIFICATION_TOKEN, USER_SWITCHED,
        SAFE_MODE_USED
    }

    enum class EventCategory {
        TEST, INFO, TAMPER, DATA, USER
    }

    enum class EventSeverity {
        DEBUG, LOW, MEDIUM, HIGH
    }

    fun sendGardionEvent(type: GardionEventType, payload: Map<String, String?>? = null) {
        ControlWorkers.startEventWorker(context, getEventId(type), payload)
    }

    private fun getGardionEvent(type: GardionEventType, payload: JsonObject? = null): GardionEvent {
        val eventTimestamp = System.currentTimeMillis()
        val eventDescription = getEventDescription(type)
        val eventId = getEventId(type)
        val deviceId = configData.getConfigurationDeviceId()
        val eventCategory = getEventCategory(type).toString().toLowerCase()
        val eventSeverity = getEventSeverity(type).toString().toLowerCase()
        val event = GardionEvent.Event(eventTimestamp, eventDescription, eventId, eventCategory,
                eventSeverity, deviceId.toString(), payload)
        return GardionEvent(event)
    }

    private val eventTypIdMap = mapOf(
            GardionEventType.ADMIN_DEACTIVATION to context.getString(R.string.event_disable_admin_id),
            GardionEventType.VPN_DISCONNECTED to context.getString(R.string.event_vpn_disconnected_id),
            GardionEventType.VPN_REMOVED to context.getString(R.string.event_vpn_removed_id),
            GardionEventType.VPN_INTERVAL_CHECK to context.getString(R.string.event_interval_check_id),
            GardionEventType.VPN_CONNECTED to context.getString(R.string.event_vpn_connected_id),
            GardionEventType.VPN_DEACTIVATED to context.getString(R.string.event_vpn_deactivated_id),
            GardionEventType.TEST_DEV to context.getString(R.string.event_test_dev_id),
            GardionEventType.ADMIN_ACTIVATION to context.getString(R.string.event_enable_admin_id),
            GardionEventType.PACKAGE_ADDED to context.getString(R.string.event_package_added_id),
            GardionEventType.USAGE_STATS to context.getString(R.string.event_usage_stats_id),
            GardionEventType.POWER_DISCONNECTED to context.getString(R.string.event_power_disconnected_id),
            GardionEventType.DEVICE_EXISTS_BACKEND to context.getString(R.string.event_device_exists_backend_id),
            GardionEventType.GARDION_RESET to context.getString(R.string.event_gardion_reset_id),
            GardionEventType.CERTIFICATE_REMOVED to context.getString(R.string.event_certificate_removed_id),
            GardionEventType.NO_USAGE_STATS_PERMISSION to context.getString(R.string.event_no_usage_stats_permission_id),
            GardionEventType.REPORT_URL to context.getString(R.string.event_report_url_id),
            GardionEventType.PUSH_NOTIFICATION_TOKEN to context.getString(R.string.event_push_notification_token_id),
            GardionEventType.USER_SWITCHED to context.getString(R.string.event_user_switched_id),
            GardionEventType.SAFE_MODE_USED to context.getString(R.string.event_safe_mode_used_id)
    )

    fun getEventType(eventId: String): GardionEventType {
        val bar = eventTypIdMap.filterValues { it == eventId }.keys
        return bar.iterator().next()
    }

    private fun getEventId(type: GardionEventType): String {
        return eventTypIdMap[type] ?: error("no event ID defined for type $type")
    }

    private fun getEventDescription(type: GardionEventType): String {
        return when (type) {
            GardionEventType.ADMIN_DEACTIVATION -> context.getString(R.string.event_disable_admin_desc)
            GardionEventType.VPN_DISCONNECTED -> context.getString(R.string.event_vpn_disconnected_desc)
            GardionEventType.VPN_REMOVED -> context.getString(R.string.event_vpn_removed_desc)
            GardionEventType.VPN_INTERVAL_CHECK -> context.getString(R.string.event_interval_check_desc)
            GardionEventType.VPN_CONNECTED -> context.getString(R.string.event_vpn_connected_desc)
            GardionEventType.VPN_DEACTIVATED -> context.getString(R.string.event_vpn_deactivated)
            GardionEventType.TEST_DEV -> context.getString(R.string.event_test_dev_desc)
            GardionEventType.ADMIN_ACTIVATION -> context.getString(R.string.event_enable_admin_desc)
            GardionEventType.PACKAGE_ADDED -> context.getString(R.string.event_package_added_desc)
            GardionEventType.USAGE_STATS -> context.getString(R.string.event_usage_stats_desc)
            GardionEventType.POWER_DISCONNECTED -> context.getString(R.string.event_power_disconnected_desc)
            GardionEventType.DEVICE_EXISTS_BACKEND -> context.getString(R.string.event_device_exists_desc)
            GardionEventType.GARDION_RESET -> context.getString(R.string.event_gardion_reset_desc)
            GardionEventType.CERTIFICATE_REMOVED -> context.getString(R.string.event_certificate_removed_desc)
            GardionEventType.NO_USAGE_STATS_PERMISSION -> context.getString(R.string.event_no_usage_stats_permission_desc)
            GardionEventType.REPORT_URL -> context.getString(R.string.event_report_url_desc)
            GardionEventType.PUSH_NOTIFICATION_TOKEN -> context.getString(R.string.event_push_notification_token_desc)
            GardionEventType.USER_SWITCHED -> context.getString(R.string.event_user_switched_desc)
            GardionEventType.SAFE_MODE_USED -> context.getString(R.string.event_safe_mode_used_desc)
        }
    }

    private fun getEventCategory(type: GardionEventType): EventCategory {
        return when (type) {
            GardionEventType.ADMIN_DEACTIVATION -> EventCategory.TAMPER
            GardionEventType.VPN_DISCONNECTED -> EventCategory.INFO
            GardionEventType.VPN_REMOVED -> EventCategory.INFO
            GardionEventType.VPN_INTERVAL_CHECK -> EventCategory.INFO
            GardionEventType.VPN_CONNECTED -> EventCategory.INFO
            GardionEventType.VPN_DEACTIVATED -> EventCategory.INFO
            GardionEventType.TEST_DEV -> EventCategory.TEST
            GardionEventType.ADMIN_ACTIVATION -> EventCategory.INFO
            GardionEventType.PACKAGE_ADDED -> EventCategory.USER
            GardionEventType.USAGE_STATS -> EventCategory.DATA
            GardionEventType.POWER_DISCONNECTED -> EventCategory.USER
            GardionEventType.DEVICE_EXISTS_BACKEND -> EventCategory.INFO
            GardionEventType.GARDION_RESET -> EventCategory.USER
            GardionEventType.CERTIFICATE_REMOVED -> EventCategory.TAMPER
            GardionEventType.NO_USAGE_STATS_PERMISSION -> EventCategory.INFO
            GardionEventType.REPORT_URL -> EventCategory.USER
            GardionEventType.PUSH_NOTIFICATION_TOKEN -> EventCategory.DATA
            GardionEventType.USER_SWITCHED -> EventCategory.TAMPER
            GardionEventType.SAFE_MODE_USED -> EventCategory.TAMPER
        }
    }

    private fun getEventSeverity(type: GardionEventType): EventSeverity {
        return when (type) {
            GardionEventType.ADMIN_DEACTIVATION -> EventSeverity.HIGH
            GardionEventType.VPN_DISCONNECTED -> EventSeverity.LOW
            GardionEventType.VPN_REMOVED -> EventSeverity.MEDIUM
            GardionEventType.VPN_INTERVAL_CHECK -> EventSeverity.LOW
            GardionEventType.VPN_CONNECTED -> EventSeverity.LOW
            GardionEventType.VPN_DEACTIVATED -> EventSeverity.HIGH
            GardionEventType.TEST_DEV -> EventSeverity.DEBUG
            GardionEventType.ADMIN_ACTIVATION -> EventSeverity.LOW
            GardionEventType.PACKAGE_ADDED -> EventSeverity.MEDIUM
            GardionEventType.USAGE_STATS -> EventSeverity.LOW
            GardionEventType.POWER_DISCONNECTED -> EventSeverity.LOW
            GardionEventType.DEVICE_EXISTS_BACKEND -> EventSeverity.LOW
            GardionEventType.GARDION_RESET -> EventSeverity.LOW
            GardionEventType.CERTIFICATE_REMOVED -> EventSeverity.MEDIUM
            GardionEventType.NO_USAGE_STATS_PERMISSION -> EventSeverity.HIGH
            GardionEventType.REPORT_URL -> EventSeverity.MEDIUM
            GardionEventType.PUSH_NOTIFICATION_TOKEN -> EventSeverity.MEDIUM
            GardionEventType.USER_SWITCHED -> EventSeverity.HIGH
            GardionEventType.SAFE_MODE_USED -> EventSeverity.HIGH
        }
    }

    // only for workers
    fun sendEventForWorker(type: GardionEventType, payload: JsonObject? = null): Boolean {
        Log.i("GARDION_EVENT", "trying to send out event...")
        return try {
            val response = api.postEvent(getGardionEvent(type, payload)).execute()
            if (response.isSuccessful) {
                Log.i("GARDION_EVENT", "sent out: ${getGardionEvent(type, payload)}")
                true
            } else {
                Log.w("GARDION_EVENT", "failed to post on server with response code " +
                        "${response.code()}, full response $response")
                false
            }
        } catch (e: Exception) {
            Log.w("GARDION_EVENT", "failed to post on server with exception: " + e.message)
            false
        }
    }

    fun isDeviceInBackend(): Boolean {
        val response = api.postEvent(
                getGardionEvent(GardionEventType.DEVICE_EXISTS_BACKEND, null)).execute()
        return response.code() != 404
    }
}