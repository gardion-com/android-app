package com.gardion.android.family.client.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.gardion.android.family.client.R
import com.gardion.android.family.client.ui.PopupBlockActivity
import com.gardion.android.family.client.logic.FlowController


class NotificationManager(private val context: Context) {

    private var logTag = "GARDION_NOTIFICATION"

    enum class GardionNotificationType {
        DEVICE_LOCKED, ADMIN_DEACTIVATED, GARDION_DEACTIVATED, CERTIFICATE_REMOVED,
        START_VPN_FOREGROUND, PUSH_NOTIFICATION
    }

    enum class GardionNotificationChannel {
        GENERAL, WARNING, PUSH, VPN
    }

    // creates notification by type
    fun createNotification(type: GardionNotificationType, title: String?, text: String?): Notification? {
        Log.d(logTag, "created notification of type $type")
        val builder = NotificationCompat.Builder(context,
                getNotificationChannelId(type))
                .setContentTitle(getNotificationContentTitle(type, title))
                .setContentText(getNotificationContentText(type, text))
                .setSubText(getNotificationContentSubText(type))
                .setSmallIcon(getNotificationSmallIcon(type))
                .setCategory(getNotificationCategory(type))
                .setColor(getNotificationColor(type))
                .setColorized(getNotificationColorized(type))
                .setContentIntent(getNotificationContentIntent(type))
                .setPriority(getNotificationPriority(type))
        // if text is set explicitly / non-default for type use BigTextStyle to allow longer (expandable) text
        if (text != null) {
            builder.setStyle(NotificationCompat
                    .BigTextStyle()
                    .setBigContentTitle(title)
                    .bigText(text)
            )
        }
        return builder.build()
    }

    fun cancelNotification(type: GardionNotificationType) {
        val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(getNotificationId(type))
    }

    fun cancelAllNotifications() {
        val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    fun notify(type: GardionNotificationType, title: String?, body: String?) {
        val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(getNotificationId(type), createNotification(type, title, body))
    }

    // atm we assuming there will be only one instance for each type of notification, might change in the future
    fun getNotificationId(type: GardionNotificationType): Int {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> 100
            GardionNotificationType.ADMIN_DEACTIVATED -> 101
            GardionNotificationType.GARDION_DEACTIVATED -> 102
            GardionNotificationType.CERTIFICATE_REMOVED -> 103
            GardionNotificationType.START_VPN_FOREGROUND -> 104
            GardionNotificationType.PUSH_NOTIFICATION -> 105
        }
    }

    private fun getNotificationChannel(type: GardionNotificationType): GardionNotificationChannel {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> GardionNotificationChannel.WARNING
            GardionNotificationType.ADMIN_DEACTIVATED -> GardionNotificationChannel.WARNING
            GardionNotificationType.GARDION_DEACTIVATED -> GardionNotificationChannel.WARNING
            GardionNotificationType.CERTIFICATE_REMOVED -> GardionNotificationChannel.WARNING
            GardionNotificationType.START_VPN_FOREGROUND -> GardionNotificationChannel.VPN
            GardionNotificationType.PUSH_NOTIFICATION -> GardionNotificationChannel.PUSH
        }
    }

    private fun getNotificationChannelId(type: GardionNotificationType): String {
        return getChannelId(getNotificationChannel(type))
    }

    private fun getNotificationContentTitle(type: GardionNotificationType, title: String?): String {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> context.getString(R.string.popup_notification_title)
            GardionNotificationType.ADMIN_DEACTIVATED -> context.getString(R.string.admin_deactivated_notification_title)
            GardionNotificationType.GARDION_DEACTIVATED -> context.getString(R.string.gardion_deactivated_notification_title)
            GardionNotificationType.CERTIFICATE_REMOVED -> context.getString(R.string.certificate_removed_notification_title)
            GardionNotificationType.START_VPN_FOREGROUND -> context.getString(R.string.start_vpn_foreground_notification_title)
            GardionNotificationType.PUSH_NOTIFICATION -> title ?: ""
        }
    }

    private fun getNotificationContentText(type: GardionNotificationType, text: String?): String {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> context.getString(R.string.popup_notification_text)
            GardionNotificationType.ADMIN_DEACTIVATED -> context.getString(R.string.admin_deactivated_notification_text)
            GardionNotificationType.GARDION_DEACTIVATED -> context.getString(R.string.gardion_deactivated_notification_text)
            GardionNotificationType.CERTIFICATE_REMOVED -> context.getString(R.string.certificate_removed_notification_text)
            GardionNotificationType.START_VPN_FOREGROUND -> context.getString(R.string.start_vpn_foreground_notification_text)
            GardionNotificationType.PUSH_NOTIFICATION -> text ?: ""
        }
    }

    private fun getNotificationContentSubText(type: GardionNotificationType): String {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> context.getString(R.string.popup_notification_subtext)
            else -> ""
        }
    }

    private fun getNotificationSmallIcon(type: GardionNotificationType): Int {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> R.drawable.ic_notification_warning
            GardionNotificationType.ADMIN_DEACTIVATED -> R.drawable.ic_notification_warning
            GardionNotificationType.GARDION_DEACTIVATED -> R.drawable.ic_notification_warning
            GardionNotificationType.CERTIFICATE_REMOVED -> R.drawable.ic_notification_warning
            GardionNotificationType.START_VPN_FOREGROUND -> R.drawable.ic_conn_sync
            GardionNotificationType.PUSH_NOTIFICATION -> R.drawable.ic_notification_warning
        }
    }

    private fun getNotificationCategory(type: GardionNotificationType): String {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> NotificationCompat.CATEGORY_STATUS
            GardionNotificationType.ADMIN_DEACTIVATED -> NotificationCompat.CATEGORY_STATUS
            GardionNotificationType.GARDION_DEACTIVATED -> NotificationCompat.CATEGORY_STATUS
            GardionNotificationType.CERTIFICATE_REMOVED -> NotificationCompat.CATEGORY_STATUS
            GardionNotificationType.START_VPN_FOREGROUND -> NotificationCompat.CATEGORY_STATUS
            GardionNotificationType.PUSH_NOTIFICATION -> NotificationCompat.CATEGORY_MESSAGE
        }
    }

    private fun getNotificationColor(type: GardionNotificationType): Int {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> ContextCompat.getColor(context, R.color.gardion_accent)
            GardionNotificationType.ADMIN_DEACTIVATED -> ContextCompat.getColor(context, R.color.gardion_accent)
            GardionNotificationType.GARDION_DEACTIVATED -> ContextCompat.getColor(context, R.color.gardion_accent)
            GardionNotificationType.CERTIFICATE_REMOVED -> ContextCompat.getColor(context, R.color.gardion_accent)
            GardionNotificationType.START_VPN_FOREGROUND -> ContextCompat.getColor(context, R.color.gardion_light_gray)
            GardionNotificationType.PUSH_NOTIFICATION -> ContextCompat.getColor(context, R.color.gardion_primary)
        }
    }

    private fun getNotificationColorized(type: GardionNotificationType): Boolean {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> true
            GardionNotificationType.ADMIN_DEACTIVATED -> false
            GardionNotificationType.GARDION_DEACTIVATED -> false
            GardionNotificationType.CERTIFICATE_REMOVED -> true
            GardionNotificationType.START_VPN_FOREGROUND -> false
            GardionNotificationType.PUSH_NOTIFICATION -> false
        }
    }

    private fun getNotificationContentIntent(type: GardionNotificationType): PendingIntent? {
        val intentClass = when (type) {
            GardionNotificationType.DEVICE_LOCKED -> PopupBlockActivity::class.java
            GardionNotificationType.ADMIN_DEACTIVATED -> FlowController::class.java
            GardionNotificationType.GARDION_DEACTIVATED -> FlowController::class.java
            GardionNotificationType.CERTIFICATE_REMOVED -> FlowController::class.java
            GardionNotificationType.START_VPN_FOREGROUND -> FlowController::class.java
            GardionNotificationType.PUSH_NOTIFICATION -> FlowController::class.java
        }
        val intentNotification = Intent(context, intentClass)
        intentNotification.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        return PendingIntent.getActivity(context.applicationContext,
                123, intentNotification, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    // for Android < API 25 priority of notifications must be set on notification level instead of channel level
    private fun getNotificationPriority(type: GardionNotificationType): Int {
        return when (type) {
            GardionNotificationType.DEVICE_LOCKED -> NotificationCompat.PRIORITY_HIGH
            GardionNotificationType.ADMIN_DEACTIVATED -> NotificationCompat.PRIORITY_HIGH
            GardionNotificationType.GARDION_DEACTIVATED -> NotificationCompat.PRIORITY_HIGH
            GardionNotificationType.CERTIFICATE_REMOVED -> NotificationCompat.PRIORITY_HIGH
            GardionNotificationType.START_VPN_FOREGROUND -> NotificationCompat.PRIORITY_DEFAULT
            GardionNotificationType.PUSH_NOTIFICATION -> NotificationCompat.PRIORITY_HIGH
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannel(channel: GardionNotificationChannel) {
        //leaving some values like vibration pattern here as default for now
        val notificationChannel = NotificationChannel(getChannelId(channel),
                getChannelName(channel), getChannelImportance(channel)).apply {
            description = getChannelDescription(channel)
            enableVibration(false)
            vibrationPattern = longArrayOf(0L)
            enableLights(false)
            setShowBadge(getChannelShowBadge(channel))
        }
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(notificationChannel)
        Log.d(logTag, "created notification channel of channel type $channel")
    }

    private fun getChannelId(channel: GardionNotificationChannel): String {
        return when (channel) {
            GardionNotificationChannel.GENERAL -> context.getString(R.string.notification_channel_general_id)
            GardionNotificationChannel.WARNING -> context.getString(R.string.notification_channel_warning_id)
            GardionNotificationChannel.PUSH -> context.getString(R.string.notification_channel_push_id)
            GardionNotificationChannel.VPN -> context.getString(R.string.notification_channel_vpn_id)
        }
    }

    private fun getChannelName(channel: GardionNotificationChannel): String {
        return when (channel) {
            GardionNotificationChannel.GENERAL -> context.getString(R.string.notification_channel_general_name)
            GardionNotificationChannel.WARNING -> context.getString(R.string.notification_channel_warning_name)
            GardionNotificationChannel.PUSH -> context.getString(R.string.notification_channel_push_name)
            GardionNotificationChannel.VPN -> context.getString(R.string.notification_channel_vpn_name)
        }
    }

    private fun getChannelDescription(channel: GardionNotificationChannel): String {
        return when (channel) {
            GardionNotificationChannel.GENERAL -> context.getString(R.string.notification_channel_general_description)
            GardionNotificationChannel.WARNING -> context.getString(R.string.notification_channel_warning_description)
            GardionNotificationChannel.PUSH -> context.getString(R.string.notification_channel_push_description)
            GardionNotificationChannel.VPN -> context.getString(R.string.notification_channel_vpn_description)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getChannelImportance(channel: GardionNotificationChannel): Int {
        return when (channel) {
            GardionNotificationChannel.GENERAL -> NotificationManager.IMPORTANCE_DEFAULT
            GardionNotificationChannel.WARNING -> NotificationManager.IMPORTANCE_HIGH
            GardionNotificationChannel.PUSH -> NotificationManager.IMPORTANCE_HIGH
            GardionNotificationChannel.VPN -> NotificationManager.IMPORTANCE_LOW
        }
    }

    private fun getChannelShowBadge(channel: GardionNotificationChannel): Boolean {
        return when (channel) {
            GardionNotificationChannel.VPN -> false
            else -> true
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createAllNotificationChannels() {
        enumValues<GardionNotificationChannel>().forEach {
            createNotificationChannel(it)
        }
    }
}