package com.gardion.android.family.client.utils

import android.content.Context
import android.content.Intent
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import com.gardion.android.family.client.ui.GardionVpnActivity
import com.gardion.android.family.client.network.Network

class ReceiverUtils {

    companion object {
        fun connectVpnOnReceive(context: Context?, keyStartedFrom: String) {
            if (context != null && !flowData.isGardionDeactivatedAllowed()!!) {
                if (!Utils.isVpnConnected(context)) {
                    if (Utils.isVpnReady()) {
                        if (Utils.isInternetReady(context)) {
                            val gardionIntent = Intent(context, GardionVpnActivity::class.java)
                            gardionIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            gardionIntent.putExtra(keyStartedFrom, true)
                            context.applicationContext?.startActivity(gardionIntent)
                        } else {
                            Network.requestNetworkInternet(context)
                        }
                    }
                }
            }
        }
    }
}