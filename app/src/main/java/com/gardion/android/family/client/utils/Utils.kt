package com.gardion.android.family.client.utils

import android.app.Activity
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.gardion.android.family.client.receiver.GardionDeviceAdminReceiver
import java.security.MessageDigest
import android.content.pm.PackageManager
import android.net.NetworkCapabilities.*
import android.os.Build
import android.provider.Settings
import androidx.preference.PreferenceManager
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import com.gardion.android.family.client.logic.GardionApplication.Companion.flowData
import java.util.*


class Utils {

    companion object {

        private const val logTag = "GARDION_UTILS"

        // this actually just checks if the system is connected to a (any) VPN, not specifically
        // the Gardion VPN
        fun isVpnConnected(context: Context): Boolean {
            return existsNetworkHasTransport(context, TRANSPORT_VPN)
        }

        // additionally checking if flight mode on a sexistsNetworkHasCapability seems to update
        // not fast enough
        fun isInternetReady(context: Context): Boolean {
            return existsInternetCapability(context) && hasNetworkTransportForInternet(context)
        }

        private fun existsInternetCapability(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // TODO: check if this makes sense
                existsNetworkHasCapability(context, NET_CAPABILITY_VALIDATED)
            } else {
                existsNetworkHasCapability(context, NET_CAPABILITY_INTERNET)
            }
        }

        private fun hasNetworkTransportForInternet(context: Context): Boolean {
            return existsNetworkHasTransport(context, TRANSPORT_WIFI) ||
                    existsNetworkHasTransport(context, TRANSPORT_CELLULAR) ||
                    existsNetworkHasTransport(context, TRANSPORT_ETHERNET)
        }

        private fun existsNetworkHasCapability(context: Context, capability: Int): Boolean {
            getAllNetworkCapabilities(context).forEach {
                if (it != null && it.hasCapability(capability)) {
                    return true
                }
            }
            return false
        }

        private fun existsNetworkHasTransport(context: Context, transport: Int): Boolean {
            getAllNetworkCapabilities(context).forEach {
                if (it != null && it.hasTransport(transport)) {
                    return true
                }
            }
            return false
        }

        private fun getAllNetworkCapabilities(context: Context): List<NetworkCapabilities?> {
            val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networks = connectivityManager.allNetworks
            return networks.map { connectivityManager.getNetworkCapabilities(it) }
        }

        fun forceKeyboardOpen(activity: Activity) {
            val imm: InputMethodManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }

        fun forceKeyboardOpenFragment(fragmentActivity: FragmentActivity?) {
            val imm: InputMethodManager = fragmentActivity
                    ?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }

        fun hideKeyboard(activity: Activity) {
            val imm =
                    activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun isVpnReady(): Boolean {
            return flowData.isVpnProfileSaved()!! &&
                    (!flowData.isUserCertificateUsed()!! || flowData.isUserCertificateChosen()!!)
        }

        // getRunningServices is deprecated, however it still returns the callers own services which is what we need
        // see also https://developer.android.com/reference/android/app/ActivityManager.html#getRunningServices(int)
        fun isMyServiceRunning(serviceClass: Class<*>, context: Context): Boolean {
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
            return false
        }

        fun hashSha256(string: String): String {
            val bytes = string.toByteArray()
            val md = MessageDigest.getInstance("SHA-256")
            val digest = md.digest(bytes)
            return digest.fold("") { str, it -> str + "%02x".format(it) }
        }

        fun isDeviceAdminActive(context: Context): Boolean {
            val manager =
                    context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
            return manager.isAdminActive(GardionDeviceAdminReceiver.getComponentName(context))
        }

        fun getAppLabel(context: Context, packageName: String): String? {
            Log.d(logTag, "starting...")
            val packageManager = context.packageManager
            return try {
                val appInfo =
                        packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA)
                val appLabel = packageManager.getApplicationLabel(appInfo).toString()
                appLabel
            } catch (exceptionNameNotFound: PackageManager.NameNotFoundException) {
                Log.w(logTag, "No app label found for package $packageName")
                null
            }
        }

        // check if a permission has been granted;
        // permission must be a string constant of AppOpsManager
        private fun hasSpecialPermission(context: Context, permission: String): Boolean {
            val appOpsManager =
                    context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
            val mode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                appOpsManager.unsafeCheckOpNoThrow(permission,
                        android.os.Process.myUid(), context.packageName)
            } else {
                @Suppress("DEPRECATION")
                appOpsManager.checkOpNoThrow(permission,
                        android.os.Process.myUid(), context.packageName)
            }

            return mode == AppOpsManager.MODE_ALLOWED
        }


        fun hasUsageStatsPermission(context: Context): Boolean {
            return hasSpecialPermission(context, AppOpsManager.OPSTR_GET_USAGE_STATS)
        }

        @RequiresApi(Build.VERSION_CODES.M)
        fun hasOverlayPermission(context: Context): Boolean {
            return hasSpecialPermission(context, AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW
            )
        }

        fun isGardionCodeValid(gardionCode: String): Boolean {
            return (gardionCode.length == 6)
        }

        fun calStartOfToday(): Calendar {
            val cal = Calendar.getInstance()
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.SECOND, 0)
            cal.set(Calendar.MILLISECOND, 0)
            return cal
        }

        fun calEndOfToday(): Calendar {
            val cal = Calendar.getInstance()
            cal.set(Calendar.HOUR_OF_DAY, 23)
            cal.set(Calendar.MINUTE, 59)
            cal.set(Calendar.SECOND, 59)
            cal.set(Calendar.MILLISECOND, 0)
            return cal
        }

        fun isPushNotificationEnabled(context: Context): Boolean {
            return PreferenceManager.getDefaultSharedPreferences(context)
                    .getBoolean("@string/pref_key_push_notification_enabled", false)
        }

        private fun isFlightModeOn(context: Context): Boolean {
            val mode = Settings.Global.getInt(context.contentResolver,
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0
            Log.d("TEST_DEV", "flight mode: $mode")
            return mode
        }
    }
}